using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SudokuSolver
{
    internal sealed class Form1 : Form
    {
        internal const int CellSize = 30;

        internal const string FileFilter =
            Program.Name + "ファイル|*." + Program.FileExtension +
            "|All Files|*.*";

        static readonly Pen Cursor3Pen = new Pen(Color.Red, 3) {
            Alignment = PenAlignment.Center
        };

        private Problem problem;
        private Point cursorLocation;
        private Size offset = new Size(20, 20);
        private Cell targetCell = null;
        private bool editMode = true;
        private Action cancelSearch = null;
        private bool moveByKeyboardScrolling = false;
        private string fileName = "";
        private bool _edited = false;

        internal Form1() : base()
        {
            this.InitializeComponent();
        }

        private void setNewProblem(IEnumerable<SudokuFrame> frames)
        {
            this.fileName = "";
            this.editMode = true;
            this.problem = new Problem();
            this.problem.AddFrames(frames);
            this.problem.SetUp();
            this.pictureBox1.Image = null;
            this.pictureBox1.Size = this.problem.Bounds.Size + this.offset + this.offset;
            this.pictureBox1.Refresh();
            this.toolStripMenuItem9.Enabled = false;
            this.toolStripMenuItem13.Enabled = false;
            this.toolStripStatusLabel1.Text = "";
            this.Edited = false;
            if (this.pictureBox1.CanFocus)
            {
                this.pictureBox1.Focus();
            }
            if (this.panel1.CanFocus)
            {
                this.panel1.Focus();
            }
        }

        private bool Edited
        {
            get
            {
                return this._edited;
            }
            set
            {
                if (this._edited && value)
                {
                    return;
                }
                this._edited = value;
                var title = Program.Name;
                if (this.fileName != "")
                {
                    title += " [ " + Path.GetFileName(this.fileName) + " ]";
                }
                if (this._edited)
                {
                    title += " *";
                }
                this.Text = title;
            }
        }

        private bool confirmKeepEditing()
        {
            if (!this.Edited)
            {
                return false;
            }
            var res = MessageBox.Show(
                "変更が保存されていません\r\n" +
                "変更を破棄します",
                Program.Name,
                MessageBoxButtons.OKCancel
            );
            if (res == DialogResult.Cancel)
            {
                return true;
            }
            return false;
        }

        private void setUpContextMenuStrip1(Cell cell)
        {
            this.toolStripMenuItem6.Checked = cell.Marked;
            if (cell.Fixed)
            {
                this.toolStripMenuItem1.Enabled = false;
                this.toolStripMenuItem2.Enabled = false;
                this.toolStripMenuItem3.Enabled = false;
                this.toolStripMenuItem4.Enabled = false;
                this.toolStripMenuItem5.Checked = true;
                this.toolStripMenuItem6.Enabled = false;
            }
            else
            {
                this.toolStripMenuItem1.Enabled = true;
                this.toolStripMenuItem4.Enabled = cell.Number > 0;
                this.toolStripMenuItem5.Checked = false;
                this.toolStripMenuItem6.Enabled = true;
                if (cell.MaxNumber < 10)
                {
                    this.toolStripMenuItem2.Enabled = false;
                    this.toolStripMenuItem3.Enabled = false;
                    for (var i = 0; i < this.toolStripMenuItem1.DropDownItems.Count; i++)
                    {
                        this.toolStripMenuItem1.DropDownItems[i].Enabled =
                            i + 1 <= cell.MaxNumber;
                    }
                }
                else if (cell.MaxNumber < 20)
                {
                    this.toolStripMenuItem2.Enabled = true;
                    this.toolStripMenuItem3.Enabled = false;
                    foreach (ToolStripItem item in this.toolStripMenuItem1.DropDownItems)
                    {
                        item.Enabled = true;
                    }
                    for (var i = 0; i < this.toolStripMenuItem2.DropDownItems.Count; i++)
                    {
                        this.toolStripMenuItem2.DropDownItems[i].Enabled =
                            i + 10 <= cell.MaxNumber;
                    }
                }
                else if (cell.MaxNumber < 30)
                {
                    this.toolStripMenuItem2.Enabled = true;
                    this.toolStripMenuItem3.Enabled = true;
                    foreach (ToolStripItem item in this.toolStripMenuItem1.DropDownItems)
                    {
                        item.Enabled = true;
                    }
                    foreach (ToolStripItem item in this.toolStripMenuItem2.DropDownItems)
                    {
                        item.Enabled = true;
                    }
                    for (var i = 0; i < this.toolStripMenuItem3.DropDownItems.Count; i++)
                    {
                        this.toolStripMenuItem3.DropDownItems[i].Enabled =
                            i + 20 <= cell.MaxNumber;
                    }
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.confirmKeepEditing())
            {
                e.Cancel = true;
                return;
            }
            if (this.cancelSearch != null)
            {
                this.cancelSearch();
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            if (this.panel1.CanFocus)
            {
                this.panel1.Focus();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Size = new Size(350, 410);

            this.setNewProblem(new SudokuFrame[]
            {
                new SudokuFrame(new Size(3, 3))
            });
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.panel1.CanFocus)
            {
                this.panel1.Focus();
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            var state = g.Save();
            g.TranslateTransform(this.offset.Width, this.offset.Height);

            this.problem.DrawBackground(g);

            if (editMode)
            {
                if (this.problem.Bounds.Contains(
                    this.cursorLocation + (Size)this.problem.Bounds.Location))
                {
                    g.DrawLine(
                        Pens.Orange,
                        0,
                        this.cursorLocation.Y,
                        this.pictureBox1.Width - this.offset.Width * 2,
                        this.cursorLocation.Y
                    );
                    g.DrawLine(
                        Pens.Orange,
                        this.cursorLocation.X,
                        0,
                        this.cursorLocation.X,
                        this.pictureBox1.Height - this.offset.Height * 2
                    );
                }
            }

            this.problem.Draw(g);

            if (editMode)
            {
                if (this.problem.Bounds.Contains(
                    this.cursorLocation + (Size)this.problem.Bounds.Location))
                {
                    g.DrawRectangle(
                        Cursor3Pen,
                        this.cursorLocation.X - this.cursorLocation.X % Form1.CellSize,
                        this.cursorLocation.Y - this.cursorLocation.Y % Form1.CellSize,
                        Form1.CellSize,
                        Form1.CellSize
                    );
                }
            }

            g.Restore(state);
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (this.editMode)
            {
                var location = e.Location - this.offset;
                this.targetCell = this.problem.GetCell(location);
                if (this.targetCell != null)
                {
                    this.setUpContextMenuStrip1(this.targetCell);
                    this.contextMenuStrip1.Show(this.pictureBox1, e.Location);
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.moveByKeyboardScrolling)
            {
                this.moveByKeyboardScrolling = false;
                return;
            }
            if (this.editMode)
            {
                var oldLocation = this.cursorLocation;
                this.cursorLocation.X = (e.X - this.offset.Width) / Form1.CellSize * Form1.CellSize + Form1.CellSize / 2;
                this.cursorLocation.Y = (e.Y - this.offset.Height) / Form1.CellSize * Form1.CellSize + Form1.CellSize / 2;
                if (oldLocation != this.cursorLocation)
                {
                    this.pictureBox1.Refresh();
                }
            }
        }

        private void editMode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!this.editMode)
            {
                return;
            }
            Cell cell = null;
            if (!Char.IsDigit(e.KeyChar))
            {
                if (e.KeyChar == 'f' || e.KeyChar == (char)Keys.Return)
                {
                    cell = this.problem.GetCell(this.cursorLocation);
                    if (cell != null)
                    {
                        cell.Fixed = !cell.Fixed;
                        this.pictureBox1.Refresh();
                        this.Edited = true;
                    }
                }
                else if (e.KeyChar == 'm' || e.KeyChar == ' ')
                {
                    cell = this.problem.GetCell(this.cursorLocation);
                    if (cell != null && !cell.Fixed)
                    {
                        cell.Marked = !cell.Marked;
                        this.pictureBox1.Refresh();
                        this.Edited = true;
                    }
                }
                return;
            }
            cell = this.problem.GetCell(this.cursorLocation);
            if (cell == null || cell.Fixed)
            {
                return;
            }
            var i = (int)Char.GetNumericValue(e.KeyChar);
            var n = cell.Number * 10 + i;
            if (n > cell.MaxNumber)
            {
                n = i;
            }
            if (n != cell.Number && 0 <= n && n <= cell.MaxNumber)
            {
                cell.Number = n;
                this.pictureBox1.Refresh();
                this.Edited = true;
            }
        }

        private void editMode_KeyDown(object sender, KeyEventArgs e)
        {
            if (!this.editMode)
            {
                return;
            }
            var oldLocation = this.cursorLocation;
            switch (e.KeyCode)
            {
                case Keys.Left:
                    if (this.cursorLocation.X - Form1.CellSize >= 0)
                    {
                        this.cursorLocation.X -= Form1.CellSize;
                    }
                    break;
                case Keys.Right:
                    if (this.cursorLocation.X + Form1.CellSize < this.pictureBox1.Width - this.offset.Width * 2)
                    {
                        this.cursorLocation.X += Form1.CellSize;
                    }
                    break;
                case Keys.Up:
                    if (this.cursorLocation.Y - Form1.CellSize >= 0)
                    {
                        this.cursorLocation.Y -= Form1.CellSize;
                    }
                    break;
                case Keys.Down:
                    if (this.cursorLocation.Y + Form1.CellSize < this.pictureBox1.Height - this.offset.Height * 2)
                    {
                        this.cursorLocation.Y += Form1.CellSize;
                    }
                    break;
                case Keys.Back:
                case Keys.Delete:
                    var cell = this.problem.GetCell(this.cursorLocation);
                    if (cell != null && cell.Number != 0 && !cell.Fixed)
                    {
                        cell.Number = 0;
                        this.pictureBox1.Refresh();
                        this.Edited = true;
                    }
                    break;
            }
            if (oldLocation == this.cursorLocation)
            {
                return;
            }
            this.pictureBox1.Refresh();
            this.resetAutoScrollPosition();
        }

        private void resetAutoScrollPosition()
        {
            var position = this.panel1.AutoScrollPosition;
            var rect = new Rectangle(
                -position.X + Form1.CellSize / 2,
                -position.Y + Form1.CellSize / 2,
                this.panel1.ClientSize.Width - Form1.CellSize,
                this.panel1.ClientSize.Height - Form1.CellSize
            );
            if (rect.Contains(this.cursorLocation + this.offset))
            {
                return;
            }
            var moveTo = new Point(
                -position.X,
                -position.Y
            );
            if (this.cursorLocation.X + this.offset.Width < rect.Left)
            {
                moveTo.X = Math.Max(
                    0,
                    moveTo.X - Form1.CellSize * (
                        1 +
                        (rect.Left - (this.cursorLocation.X + this.offset.Width)) /
                        Form1.CellSize
                    )
                );
            }
            if (this.cursorLocation.X + this.offset.Width > rect.Right)
            {
                moveTo.X = Math.Min(
                    this.pictureBox1.Width - this.panel1.ClientSize.Width,
                    moveTo.X + Form1.CellSize * (
                        1 +
                        (this.cursorLocation.X + this.offset.Width - rect.Right) /
                        Form1.CellSize
                    )
                );
            }
            if (this.cursorLocation.Y + this.offset.Height < rect.Top)
            {
                moveTo.Y = Math.Max(
                    0,
                    moveTo.Y - Form1.CellSize * (
                        1 +
                        (rect.Top - (this.cursorLocation.Y + this.offset.Height)) /
                        Form1.CellSize
                    )
                );
            }
            if (this.cursorLocation.Y + this.offset.Height > rect.Bottom)
            {
                moveTo.Y = Math.Min(
                    this.pictureBox1.Height - this.panel1.ClientSize.Height,
                    moveTo.Y + Form1.CellSize * (
                        1 +
                        (this.cursorLocation.Y + this.offset.Height - rect.Bottom) /
                        Form1.CellSize
                    )
                );
            }
            this.moveByKeyboardScrolling = true;
            this.panel1.AutoScrollPosition = moveTo;
        }

        private void cellNumbers_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (this.editMode && this.targetCell != null)
            {
                var n = (int)e.ClickedItem.Tag;
                if (this.targetCell.Number != n)
                {
                    this.targetCell.Number = n;
                    this.pictureBox1.Refresh();
                    this.Edited = true;
                }
            }
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            if (this.editMode && this.targetCell != null)
            {
                if (this.targetCell.Number != 0)
                {
                    this.targetCell.Number = 0;
                    this.pictureBox1.Refresh();
                    this.Edited = true;
                }
            }
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            if (this.editMode && this.targetCell != null)
            {
                this.targetCell.Fixed = !this.targetCell.Fixed;
                this.pictureBox1.Refresh();
                this.Edited = true;
            }
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            if (this.editMode && this.targetCell != null)
            {
                this.targetCell.Marked = !this.targetCell.Marked;
                this.pictureBox1.Refresh();
                this.Edited = true;
            }
        }

        private void toolStripMenuItem7_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            var index = this.toolStripMenuItem7.DropDownItems.IndexOf(e.ClickedItem);
            if (index < 0 || index >= 5)
            {
                return;
            }
            this.toolStripMenuItem15.HideDropDown();
            if (this.confirmKeepEditing())
            {
                return;
            }
            this.setNewProblem(new SudokuFrame[]
            {
                new SudokuFrame((Size)e.ClickedItem.Tag)
            });
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            if (this.confirmKeepEditing())
            {
                return;
            }
            string fileName = "";
            using (var dialog = new OpenFileDialog())
            {
                dialog.Filter = Form1.FileFilter;
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                fileName = dialog.FileName;
            }
            try
            {
                var problem = Package.Load(fileName);
                this.fileName = fileName;
                this.problem = problem;
                this.Edited = false;
                this.pictureBox1.Image = null;
                this.pictureBox1.Refresh();
            }
            catch
            {
                MessageBox.Show("ファイルの読み込みに失敗しました", Program.Name);
            }
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            if (this.fileName != "")
            {
                try
                {
                    Package.Save(this.fileName, this.problem);
                    this.Edited = false;
                }
                catch
                {
                    MessageBox.Show("ファイルの保存に失敗しました", Program.Name);
                }
            }
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            string fileName = "";
            using (var dialog = new SaveFileDialog())
            {
                dialog.Filter = Form1.FileFilter;
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                fileName = dialog.FileName;
            }
            try
            {
                Package.Save(fileName, this.problem);
                this.fileName = fileName;
                this.Edited = false;
            }
            catch
            {
                MessageBox.Show("ファイルの保存に失敗しました", Program.Name);
            }
        }

        private async void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            if (!problem.IsValid)
            {
                return;
            }
            this.editMode = false;
            this.toolStripMenuItem7.Enabled = false;
            this.toolStripMenuItem8.Enabled = false;
            this.toolStripMenuItem9.Enabled = false;
            this.toolStripMenuItem10.Enabled = false;
            this.toolStripMenuItem11.Enabled = false;
            this.toolStripMenuItem12.Enabled = true;
            this.toolStripMenuItem13.Enabled = false;
            this.toolStripStatusLabel1.Text = "探索中...";
            if (this.form4 != null)
            {
                this.form4.ButtonEnabled = false;
            }
            var oldCursor = this.pictureBox1.Cursor;
            this.pictureBox1.Cursor = Cursors.WaitCursor;
            using (var cancellationTokenSource = new CancellationTokenSource())
            {
                CancellationToken token = cancellationTokenSource.Token;
                this.cancelSearch = () => {
                    try
                    {
                        cancellationTokenSource.Cancel();
                    }
                    catch {}
                };
                await problem.SolveAsync(token);
                this.cancelSearch = () => {};
            }
            this.editMode = true;
            this.toolStripMenuItem7.Enabled = true;
            this.toolStripMenuItem8.Enabled = true;
            this.toolStripMenuItem9.Enabled = this.fileName != "";
            this.toolStripMenuItem10.Enabled = true;
            this.toolStripMenuItem11.Enabled = true;
            this.toolStripMenuItem12.Enabled = false;
            this.toolStripMenuItem13.Enabled = this.problem.HasSolution;
            this.toolStripStatusLabel1.Text = String.Format("探索終了 {0}", this.problem.Result);
            this.pictureBox1.Cursor = oldCursor;
            this.pictureBox1.Refresh();
            if (this.form4 != null)
            {
                this.form4.ButtonEnabled = true;
            }
        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            if (this.cancelSearch != null)
            {
                this.cancelSearch();
            }
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            if (this.editMode && this.problem.HasSolution)
            {
                this.problem.RemoveSolution();
                this.toolStripMenuItem13.Enabled = false;
                this.pictureBox1.Refresh();
            }
        }

        private void toolStripMenuItem14_Click(object sender, EventArgs e)
        {
            if (this.confirmKeepEditing())
            {
                return;
            }
            this._edited = false;
            if (this.cancelSearch != null)
            {
                this.cancelSearch();
            }
            Application.Exit();
        }

        private void toolStripMenuItem15_DropDownOpening(object sender, EventArgs e)
        {
            this.toolStripMenuItem9.Enabled = this.editMode && this.fileName != "";
            this.toolStripMenuItem20.Enabled = this.editMode && this.form4 == null;        }

        private void toolStripMenuItem16_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.toolStripMenuItem15.HideDropDown();
            if (this.confirmKeepEditing())
            {
                return;
            }
            List<SudokuFrame> frames = null;
            using (var dialog = new Form2((Size)e.ClickedItem.Tag))
            {
                var res = dialog.ShowDialog();
                if (res == DialogResult.Cancel)
                {
                    return;
                }
                frames = dialog.GetFrames();
            }
            this.setNewProblem(frames);
        }

        private void toolStripMenuItem17_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.toolStripMenuItem15.HideDropDown();
            if (this.confirmKeepEditing())
            {
                return;
            }
            this.setNewProblem(new SudokuFrame[]
            {
                new SudokuFrame(SudokuType.Diagonal, (Size)e.ClickedItem.Tag)
            });
        }

        private void toolStripMenuItem18_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.toolStripMenuItem15.HideDropDown();
            if (this.confirmKeepEditing())
            {
                return;
            }
            this.setNewProblem(new SudokuFrame[]
            {
                new SudokuFrame(SudokuType.Colored, (Size)e.ClickedItem.Tag)
            });
        }

        private void toolStripMenuItem19_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.toolStripMenuItem15.HideDropDown();
            if (this.confirmKeepEditing())
            {
                return;
            }
            SudokuFrame frame = null;
            using (var dialog = new Form3((int)e.ClickedItem.Tag))
            {
                var res = dialog.ShowDialog();
                if (res == DialogResult.Cancel)
                {
                    return;
                }
                frame = dialog.Frame;
            }
            this.setNewProblem(new SudokuFrame[] { frame });
        }

        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            this.toolStripMenuItem20.Enabled = false;
            this.form4 = new Form4(this.pictureBox1);
            this.form4.Disposed += (s1, e1) => {
                this.form4 = null;
            };
            this.form4.Show(this);
        }

        private void InitializeComponent()
        {
            this.pictureBox1 = new PictureBox();
            this.panel1 = new Panel();
            this.contextMenuStrip1 = new ContextMenuStrip();
            this.menuStrip1 = new MenuStrip();
            this.statusStrip1 = new StatusStrip();
            this.toolStripStatusLabel1 = new ToolStripStatusLabel();
            this.toolStripMenuItem1 = new ToolStripMenuItem();
            this.toolStripMenuItem2 = new ToolStripMenuItem();
            this.toolStripMenuItem3 = new ToolStripMenuItem();
            this.toolStripMenuItem4 = new ToolStripMenuItem();
            this.toolStripMenuItem5 = new ToolStripMenuItem();
            this.toolStripMenuItem6 = new ToolStripMenuItem();
            this.toolStripMenuItem7 = new ToolStripMenuItem();
            this.toolStripMenuItem8 = new ToolStripMenuItem();
            this.toolStripMenuItem9 = new ToolStripMenuItem();
            this.toolStripMenuItem10 = new ToolStripMenuItem();
            this.toolStripMenuItem11 = new ToolStripMenuItem();
            this.toolStripMenuItem12 = new ToolStripMenuItem();
            this.toolStripMenuItem13 = new ToolStripMenuItem();
            this.toolStripMenuItem14 = new ToolStripMenuItem();
            this.toolStripMenuItem15 = new ToolStripMenuItem();
            this.toolStripMenuItem16 = new ToolStripMenuItem();
            this.toolStripMenuItem17 = new ToolStripMenuItem();
            this.toolStripMenuItem18 = new ToolStripMenuItem();
            this.toolStripMenuItem19 = new ToolStripMenuItem();
            this.toolStripMenuItem20 = new ToolStripMenuItem();

            this.panel1.SuspendLayout();
            this.SuspendLayout();

            for (var i = 1; i <= 9; i++)
            {
                this.toolStripMenuItem1.DropDownItems.Add(
                    String.Format("&{0}", i)
                ).Tag = i;
            }
            this.toolStripMenuItem1.Text = "1 - 9(&0)";
            this.toolStripMenuItem1.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.cellNumbers_DropDownItemClicked);

            for (var i = 0; i <= 9; i++)
            {
                this.toolStripMenuItem2.DropDownItems.Add(
                    String.Format("1&{0}", i)
                ).Tag = 10 + i;
            }
            this.toolStripMenuItem2.Text = "10 - 19(&1)";
            this.toolStripMenuItem2.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.cellNumbers_DropDownItemClicked);

            for (var i = 0; i <= 5; i++)
            {
                this.toolStripMenuItem3.DropDownItems.Add(
                    String.Format("2&{0}", i)
                ).Tag = 20 + i;
            }
            this.toolStripMenuItem3.Text = "20 - 25(&2)";
            this.toolStripMenuItem3.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.cellNumbers_DropDownItemClicked);

            this.toolStripMenuItem4.Text = "削除(&D)";
            this.toolStripMenuItem4.Click += new EventHandler(this.toolStripMenuItem4_Click);

            this.toolStripMenuItem5.Text = "固定(&F)";
            this.toolStripMenuItem5.Click += new EventHandler(this.toolStripMenuItem5_Click);

            this.toolStripMenuItem6.Text = "マーク(&M)";
            this.toolStripMenuItem6.Click += new EventHandler(this.toolStripMenuItem6_Click);

            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem1);
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem2);
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem3);
            this.contextMenuStrip1.Items.Add("-");
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem4);
            this.contextMenuStrip1.Items.Add("-");
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem5);
            this.contextMenuStrip1.Items.Add("-");
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem6);
            this.contextMenuStrip1.Items.Add("-");
            this.contextMenuStrip1.Items.Add("キャンセル(&C)");

            this.toolStripMenuItem16.DropDownItems.Add(" 9 x  9 (3 x 3 block)").Tag = new Size(3, 3);
            this.toolStripMenuItem16.DropDownItems.Add("12 x 12 (4 x 3 block)").Tag = new Size(4, 3);
            this.toolStripMenuItem16.DropDownItems.Add("16 x 16 (4 x 4 block)").Tag = new Size(4, 4);
            this.toolStripMenuItem16.Text = "合体(&U)";
            this.toolStripMenuItem16.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.toolStripMenuItem16_DropDownItemClicked);

            this.toolStripMenuItem17.DropDownItems.Add(" 9 x  9 (3 x 3 block)...").Tag = new Size(3, 3);
            this.toolStripMenuItem17.DropDownItems.Add("16 x 16 (4 x 4 block)...").Tag = new Size(4, 4);
            this.toolStripMenuItem17.DropDownItems.Add("25 x 25 (5 x 5 block)...").Tag = new Size(5, 5);
            this.toolStripMenuItem17.Text = "対角線(&D)";
            this.toolStripMenuItem17.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.toolStripMenuItem17_DropDownItemClicked);

            this.toolStripMenuItem18.DropDownItems.Add(" 9 x  9 (3 x 3 block)").Tag = new Size(3, 3);
            this.toolStripMenuItem18.DropDownItems.Add("12 x 12 (4 x 3 block)").Tag = new Size(4, 3);
            this.toolStripMenuItem18.DropDownItems.Add("16 x 16 (4 x 4 block)").Tag = new Size(4, 4);
            this.toolStripMenuItem18.DropDownItems.Add("20 x 20 (5 x 4 block)").Tag = new Size(5, 4);
            this.toolStripMenuItem18.DropDownItems.Add("25 x 25 (5 x 5 block)").Tag = new Size(5, 5);
            this.toolStripMenuItem18.Text = "カラー(&C)";
            this.toolStripMenuItem18.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.toolStripMenuItem18_DropDownItemClicked);

            this.toolStripMenuItem19.DropDownItems.Add(" 9 x  9...").Tag = 9;
            this.toolStripMenuItem19.DropDownItems.Add("12 x 12...").Tag = 12;
            this.toolStripMenuItem19.DropDownItems.Add("16 x 16...").Tag = 16;
            this.toolStripMenuItem19.DropDownItems.Add("20 x 20...").Tag = 20;
            this.toolStripMenuItem19.DropDownItems.Add("25 x 25...").Tag = 25;
            this.toolStripMenuItem19.Text = "不定形(&F)";
            this.toolStripMenuItem19.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.toolStripMenuItem19_DropDownItemClicked);

            this.toolStripMenuItem7.DropDownItems.Add(" 9 x  9 (3 x 3 block)").Tag = new Size(3, 3);
            this.toolStripMenuItem7.DropDownItems.Add("12 x 12 (4 x 3 block)").Tag = new Size(4, 3);
            this.toolStripMenuItem7.DropDownItems.Add("16 x 16 (4 x 4 block)").Tag = new Size(4, 4);
            this.toolStripMenuItem7.DropDownItems.Add("20 x 20 (5 x 4 block)").Tag = new Size(5, 4);
            this.toolStripMenuItem7.DropDownItems.Add("25 x 25 (5 x 5 block)").Tag = new Size(5, 5);
            this.toolStripMenuItem7.DropDownItems.Add("-");
            this.toolStripMenuItem7.DropDownItems.Add(this.toolStripMenuItem16);
            this.toolStripMenuItem7.DropDownItems.Add("-");
            this.toolStripMenuItem7.DropDownItems.Add(this.toolStripMenuItem17);
            this.toolStripMenuItem7.DropDownItems.Add("-");
            this.toolStripMenuItem7.DropDownItems.Add(this.toolStripMenuItem18);
            this.toolStripMenuItem7.DropDownItems.Add("-");
            this.toolStripMenuItem7.DropDownItems.Add(this.toolStripMenuItem19);
            this.toolStripMenuItem7.Text = "新規(&N)";
            this.toolStripMenuItem7.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.toolStripMenuItem7_DropDownItemClicked);

            this.toolStripMenuItem8.Text = "開く(&O)...";
            this.toolStripMenuItem8.Click += new EventHandler(this.toolStripMenuItem8_Click);

            this.toolStripMenuItem9.Text = "上書き保存(&S)";
            this.toolStripMenuItem9.Enabled = false;
            this.toolStripMenuItem9.Click += new EventHandler(this.toolStripMenuItem9_Click);

            this.toolStripMenuItem10.Text = "名前を付けて保存(&A)...";
            this.toolStripMenuItem10.Click += new EventHandler(this.toolStripMenuItem10_Click);

            this.toolStripMenuItem11.Text = "解答探索開始(&R)";
            this.toolStripMenuItem11.Click += new EventHandler(this.toolStripMenuItem11_Click);

            this.toolStripMenuItem12.Text = "解答探索停止(&P)";
            this.toolStripMenuItem12.Enabled = false;
            this.toolStripMenuItem12.Click += new EventHandler(this.toolStripMenuItem12_Click);

            this.toolStripMenuItem13.Text = "解答除去(&C)";
            this.toolStripMenuItem13.Enabled = false;
            this.toolStripMenuItem13.Click += new EventHandler(this.toolStripMenuItem13_Click);

            this.toolStripMenuItem14.Text = "終了(&X)";
            this.toolStripMenuItem14.Click += new EventHandler(this.toolStripMenuItem14_Click);

            this.toolStripMenuItem20.Text = "補助背景(&H)...";
            this.toolStripMenuItem20.Click += new EventHandler(this.toolStripMenuItem20_Click);

            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem7);
            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem8);
            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem9);
            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem10);
            this.toolStripMenuItem15.DropDownItems.Add("-");
            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem11);
            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem12);
            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem13);
            this.toolStripMenuItem15.DropDownItems.Add("-");
            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem20);
            this.toolStripMenuItem15.DropDownItems.Add("-");
            this.toolStripMenuItem15.DropDownItems.Add(this.toolStripMenuItem14);
            this.toolStripMenuItem15.Text = "メニュー";
            this.toolStripMenuItem15.DropDownOpening +=
                new EventHandler(this.toolStripMenuItem15_DropDownOpening);

            this.menuStrip1.Items.Add(this.toolStripMenuItem15);
            this.menuStrip1.Dock = DockStyle.Top;

            this.statusStrip1.Items.Add(toolStripStatusLabel1);
            this.statusStrip1.Dock = DockStyle.Bottom;

            this.pictureBox1.Size = new Size(400, 400);
            this.pictureBox1.BackColor = Color.White;
            this.pictureBox1.Paint += new PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseClick += new MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.pictureBox1.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Top = this.menuStrip1.Bottom;
            this.panel1.Size = new Size(
                this.ClientSize.Width,
                this.ClientSize.Height -
                this.menuStrip1.Height -
                this.statusStrip1.Height
            );
            this.panel1.AutoScroll = true;
            this.panel1.Anchor =
                AnchorStyles.Left |
                AnchorStyles.Right |
                AnchorStyles.Top |
                AnchorStyles.Bottom;
            this.panel1.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.panel1.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = Program.Name;
            this.FormClosing += new FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new EventHandler(this.Form1_Load);
            this.Shown += new EventHandler(this.Form1_Shown);
            this.SizeChanged += new EventHandler(this.Form1_SizeChanged);
            this.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private Form4 form4 = null;
        private PictureBox pictureBox1;
        private Panel panel1;
        private ContextMenuStrip contextMenuStrip1;
        private MenuStrip menuStrip1;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripMenuItem toolStripMenuItem3;
        private ToolStripMenuItem toolStripMenuItem4;
        private ToolStripMenuItem toolStripMenuItem5;
        private ToolStripMenuItem toolStripMenuItem6;
        private ToolStripMenuItem toolStripMenuItem7;
        private ToolStripMenuItem toolStripMenuItem8;
        private ToolStripMenuItem toolStripMenuItem9;
        private ToolStripMenuItem toolStripMenuItem10;
        private ToolStripMenuItem toolStripMenuItem11;
        private ToolStripMenuItem toolStripMenuItem12;
        private ToolStripMenuItem toolStripMenuItem13;
        private ToolStripMenuItem toolStripMenuItem14;
        private ToolStripMenuItem toolStripMenuItem15;
        private ToolStripMenuItem toolStripMenuItem16;
        private ToolStripMenuItem toolStripMenuItem17;
        private ToolStripMenuItem toolStripMenuItem18;
        private ToolStripMenuItem toolStripMenuItem19;
        private ToolStripMenuItem toolStripMenuItem20;
    }
}