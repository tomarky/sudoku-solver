using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;

namespace SudokuSolver
{
    internal sealed class Problem
    {
        internal Rectangle CellBounds { get; private set; }
        internal Rectangle Bounds { get; private set; }
        internal IList<Cell> Cells { get; private set; }
        internal IList<SudokuFrame> Frames { get; private set; }
        internal IList<CellGroup> CellGroups { get; private set; }
        internal FillResult Result { get; private set; }

        private int[] numbers = null;
        private bool hasSetUp = false;

        internal Problem()
        {
            this.Frames = new List<SudokuFrame>();
        }

        internal bool HasSolution
        {
            get
            {
                return this.numbers != null;
            }
        }

        internal void RemoveSolution()
        {
            this.numbers = null;
        }

        internal void AddFrame(SudokuFrame frame)
        {
            if (this.hasSetUp)
            {
                throw new Exception("has set up");
            }
            if (this.Frames.Count == 0)
            {
                this.Bounds = frame.Bounds;
                this.CellBounds = frame.CellBounds;
            }
            else
            {
                this.Bounds = Rectangle.Union(this.Bounds, frame.Bounds);
                this.CellBounds = Rectangle.Union(this.CellBounds, frame.CellBounds);
            }
            this.Frames.Add(frame);
        }

        internal void AddFrames(IEnumerable<SudokuFrame> frames)
        {
            foreach (var frame in frames)
            {
                this.AddFrame(frame);
            }
        }

        internal void SetUp()
        {
            if (this.hasSetUp)
            {
                throw new Exception("has set up");
            }
            CellGroup.Reset();
            var cells = new List<Cell>();
            for (var row = this.CellBounds.Top; row < this.CellBounds.Bottom; row++)
            {
                for (var col = this.CellBounds.Left; col < this.CellBounds.Right; col++)
                {
                    Cell cell = null;
                    foreach (var f in this.Frames)
                    {
                        if (!f.CellBounds.Contains(col, row))
                        {
                            continue;
                        }
                        if (cell == null)
                        {
                            var index = cells.Count;
                            cell = new Cell(index, new Point(col, row));
                            cells.Add(cell);
                        }
                        f.Cells.Add(cell);
                    }
                }
            }
            this.Cells = cells.AsReadOnly();
            foreach (var f in this.Frames)
            {
                f.SetUp();
            }
            this.Frames = ((List<SudokuFrame>)this.Frames).AsReadOnly();
            this.CellGroups = CellGroup.GetGroups();
            this.hasSetUp = true;
        }

        internal Cell GetCell(Point location)
        {
            location += (Size)this.Bounds.Location;
            if (!this.Bounds.Contains(location))
            {
                return null;
            }
            foreach (var f in this.Frames)
            {
                var cell = f.GetCell(location);
                if (cell != null)
                {
                    return cell;
                }
            }
            return null;
        }

        internal bool IsValid
        {
            get
            {
                if (!this.hasSetUp)
                {
                    return false;
                }
                if (this.Cells == null)
                {
                    return false;
                }
                foreach (var cell in this.Cells)
                {
                    if (!cell.IsValid)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        internal void DrawBackground(Graphics g)
        {
            var state = g.Save();
            g.TranslateTransform(-this.Bounds.X, -this.Bounds.Y);
            foreach (var f in this.Frames)
            {
                f.DrawBackground(g);
            }
            if (this.Cells != null)
            {
                foreach (var c in this.Cells)
                {
                    c.DrawBackground(g);
                }
            }
            g.Restore(state);
        }

        internal void Draw(Graphics g)
        {
            var state = g.Save();
            g.TranslateTransform(-this.Bounds.X, -this.Bounds.Y);
            if (this.Cells != null)
            {
                if (this.numbers != null)
                {
                    foreach (var c in this.Cells)
                    {
                        c.Draw(g, this.numbers[c.Index]);
                    }
                }
                else
                {
                    foreach (var c in this.Cells)
                    {
                        c.Draw(g);
                    }
                }
            }
            foreach (var f in this.Frames)
            {
                f.Draw(g);
            }
            g.Restore(state);
        }

        internal async Task SolveAsync(CancellationToken token)
        {
            this.numbers = null;
            var solver = new Solver(this);
            var result = await Task.Run( () => solver.Solve(token) );
            this.Result = result;
            if (result == FillResult.Filled)
            {
                foreach (var c in this.Cells)
                {
                    if (c.IsEmpty && !solver.IsEmptyCell(c))
                    {
                        this.numbers = solver.Numbers;
                        break;
                    }
                }
            }
        }
    }
}