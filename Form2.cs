using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SudokuSolver
{
    internal sealed class Form2 : Form
    {
        const int CellSize = 6;

        private readonly Size offset = new Size(20, 20);
        private readonly Size blockSize;
        private readonly int maxNumber;

        private readonly List<Point> frames = new List<Point>();

        private Point cursorLocation = new Point(0, 0);

        private bool firstTime = true;
        private bool blockUnit = true;
        private bool moveByKeyboardScrolling = false;

        internal List<SudokuFrame> GetFrames()
        {
            var ret = new List<SudokuFrame>(this.frames.Count);
            foreach (var p in this.frames)
            {
                ret.Add(new SudokuFrame(p, this.blockSize));
            }
            return ret;
        }

        internal Form2(Size blockSize) : base()
        {
            this.blockSize = blockSize;
            this.maxNumber = blockSize.Width * blockSize.Height;
            this.InitializeComponent();
        }

        private void Form2_Shown(object sender, EventArgs e)
        {
            if (this.firstTime)
            {
                this.firstTime = false;
                this.panel1.AutoScrollPosition = new Point(
                    (this.cursorLocation.X + this.maxNumber / 2) * Form2.CellSize
                        + this.offset.Width
                        - this.panel1.ClientSize.Width / 2,
                    (this.cursorLocation.Y + this.maxNumber / 2) * Form2.CellSize
                        + this.offset.Height
                        - this.panel1.ClientSize.Height / 2
                );
            }

            if (this.panel1.CanFocus)
            {
                this.panel1.Focus();
            }
        }

        private void resetFrames()
        {
            this.frames.Clear();
            var p = new Point(
                this.maxNumber * 3,
                this.maxNumber * 3
            );
            this.frames.Add(p);
            this.panel1.AutoScrollPosition = new Point(
                (p.X + this.maxNumber / 2) * Form2.CellSize
                    + this.offset.Width
                    - this.panel1.ClientSize.Width / 2,
                (p.Y + this.maxNumber / 2) * Form2.CellSize
                    + this.offset.Height
                    - this.panel1.ClientSize.Height / 2
            );
            this.pictureBox1.Refresh();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.Size = new Size(450, 500);

            var p = new Point(
                this.maxNumber * 3,
                this.maxNumber * 3
            );
            this.frames.Add(p);
            this.cursorLocation = p;
        }

        private void Form2_SizeChanged(object sender, EventArgs e)
        {
            if (this.panel1.CanFocus)
            {
                this.panel1.Focus();
            }
        }

        private void drawFrame(Graphics g, Point location, Color color)
        {
            var pen1 = new Pen(color, 1) {
                Alignment = PenAlignment.Center
            };
            var pen2 = new Pen(color, 2) {
                Alignment = PenAlignment.Center
            };
            var pen3 = new Pen(color, 3) {
                Alignment = PenAlignment.Center
            };

            var state = g.Save();
            g.TranslateTransform(location.X * Form2.CellSize, location.Y * Form2.CellSize);

            for (var i = 1; i < this.maxNumber; i++)
            {
                g.DrawLine(
                    i % this.blockSize.Height == 0 ? pen2 : pen1,
                    0,
                    i * Form2.CellSize,
                    this.maxNumber * Form2.CellSize,
                    i * Form2.CellSize
                );
                g.DrawLine(
                    i % this.blockSize.Width == 0 ? pen2 : pen1,
                    i * Form2.CellSize,
                    0,
                    i * Form2.CellSize,
                    this.maxNumber * Form2.CellSize
                );
            }

            g.DrawRectangle(
                pen3,
                0, 0,
                this.maxNumber * Form2.CellSize,
                this.maxNumber * Form2.CellSize
            );

            g.Restore(state);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            var state = g.Save();
            g.TranslateTransform(this.offset.Width, this.offset.Height);

            var exists = false;

            foreach (var p in this.frames)
            {
                this.drawFrame(g, p, Color.Black);
                if (p == this.cursorLocation)
                {
                    exists = true;
                }
            }
            if (exists)
            {
                g.DrawRectangle(
                    new Pen(Color.Red, 3) {
                        Alignment = PenAlignment.Center
                    },
                    this.cursorLocation.X * Form2.CellSize,
                    this.cursorLocation.Y * Form2.CellSize,
                    this.maxNumber * Form2.CellSize,
                    this.maxNumber * Form2.CellSize
                );
            }
            else
            {
                this.drawFrame(g, this.cursorLocation, Color.Red);
            };

            g.Restore(state);
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            this.contextMenuStrip1.Items[0].Enabled = true;
            this.contextMenuStrip1.Items[1].Enabled = false;
            foreach (var p in this.frames)
            {
                if (p == this.cursorLocation)
                {
                    this.contextMenuStrip1.Items[0].Enabled = false;
                    this.contextMenuStrip1.Items[1].Enabled = true;
                    break;
                }
            }
            this.contextMenuStrip1.Show(this.pictureBox1, e.Location);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.moveByKeyboardScrolling)
            {
                this.moveByKeyboardScrolling = false;
                return;
            }
            var loc = e.Location - this.offset;
            loc.X = Math.Max(0, loc.X / Form2.CellSize - this.blockSize.Width);
            loc.Y = Math.Max(0, loc.Y / Form2.CellSize - this.blockSize.Height);
            if (this.blockUnit)
            {
                loc.X -= loc.X % this.blockSize.Width;
                loc.Y -= loc.Y % this.blockSize.Height;
            }
            if ((loc.X + this.maxNumber) * Form2.CellSize + this.offset.Width > this.pictureBox1.Width)
            {
                return;
            }
            if ((loc.Y + this.maxNumber) * Form2.CellSize + this.offset.Height > this.pictureBox1.Height)
            {
                return;
            }
            if (loc != this.cursorLocation)
            {
                this.cursorLocation = loc;
                this.pictureBox1.Refresh();
            }
        }

        private void editMode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'd')
            {
                if (this.frames.Remove(this.cursorLocation))
                {
                    this.pictureBox1.Refresh();
                }
            }
            else if (e.KeyChar == 's' || e.KeyChar == (char)Keys.Return)
            {
                if (!this.frames.Contains(this.cursorLocation))
                {
                    this.frames.Add(this.cursorLocation);
                    this.pictureBox1.Refresh();
                }
            }
        }

        private void editMode_KeyDown(object sender, KeyEventArgs e)
        {
            var loc = this.cursorLocation;
            switch (e.KeyCode)
            {
                case Keys.Left:
                    loc.X -= this.blockUnit ? this.blockSize.Width : 1;
                    break;
                case Keys.Right:
                    loc.X += this.blockUnit ? this.blockSize.Width : 1;
                    break;
                case Keys.Up:
                    loc.Y -= this.blockUnit ? this.blockSize.Height : 1;
                    break;
                case Keys.Down:
                    loc.Y += this.blockUnit ? this.blockSize.Height : 1;
                    break;
                case Keys.Back:
                case Keys.Delete:
                    if (this.frames.Remove(loc))
                    {
                        this.pictureBox1.Refresh();
                        return;
                    }
                    break;
            }
            if (loc == this.cursorLocation)
            {
                return;
            }
            if (loc.X < 0 || loc.Y < 0)
            {
                return;
            }
            if ((loc.X + this.maxNumber) * Form2.CellSize + this.offset.Width > this.pictureBox1.Width)
            {
                return;
            }
            if ((loc.Y + this.maxNumber) * Form2.CellSize + this.offset.Height > this.pictureBox1.Height)
            {
                return;
            }
            this.cursorLocation = loc;
            this.pictureBox1.Refresh();
            this.resetAutoScrollPosition();
        }

        private void resetAutoScrollPosition()
        {
            var position = this.panel1.AutoScrollPosition;
            var rect1 = new Rectangle(
                -position.X + Form2.CellSize * this.blockSize.Width,
                -position.Y + Form2.CellSize * this.blockSize.Height,
                this.panel1.ClientSize.Width - Form2.CellSize * this.blockSize.Width * 2,
                this.panel1.ClientSize.Height - Form2.CellSize * this.blockSize.Height * 2
            );
            var rect2 = new Rectangle(
                this.cursorLocation.X * Form2.CellSize + this.offset.Width,
                this.cursorLocation.Y * Form2.CellSize + this.offset.Height,
                this.maxNumber * Form2.CellSize,
                this.maxNumber * Form2.CellSize
            );
            if (rect1.Contains(rect2))
            {
                return;
            }
            var moveTo = new Point(
                -position.X,
                -position.Y
            );
            if (rect2.Left < rect1.Left)
            {
                moveTo.X = Math.Max(
                    0,
                    moveTo.X - Form2.CellSize * this.blockSize.Width * (
                        1 +
                        (rect1.Left - rect2.Left) /
                        (Form2.CellSize * this.blockSize.Width)
                    )
                );
            }
            if (rect2.Right > rect1.Right)
            {
                moveTo.X = Math.Min(
                    this.pictureBox1.Width - this.panel1.ClientSize.Width,
                    moveTo.X + Form2.CellSize * this.blockSize.Width * (
                        1 +
                        (rect2.Right - rect1.Right) /
                        (Form2.CellSize * this.blockSize.Width)
                    )
                );
            }
            if (rect2.Top < rect1.Top)
            {
                moveTo.Y = Math.Max(
                    0,
                    moveTo.Y - Form2.CellSize * this.blockSize.Height * (
                        1 +
                        (rect1.Top - rect2.Top) /
                        (Form2.CellSize * this.blockSize.Height)
                    )
                );
            }
            if (rect2.Bottom > rect1.Bottom)
            {
                moveTo.Y = Math.Min(
                    this.pictureBox1.Height - this.panel1.ClientSize.Height,
                    moveTo.Y + Form2.CellSize * this.blockSize.Height * (
                        1 +
                        (rect2.Bottom - rect1.Bottom) /
                        (Form2.CellSize * this.blockSize.Height)
                    )
                );
            }
            this.moveByKeyboardScrolling = true;
            this.panel1.AutoScrollPosition = moveTo;
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            var index = this.contextMenuStrip1.Items.IndexOf(e.ClickedItem);
            if (index == 0)
            {
                this.frames.Add(this.cursorLocation);
                this.pictureBox1.Refresh();
            }
            else if (index == 1)
            {
                this.frames.Remove(this.cursorLocation);
                this.pictureBox1.Refresh();
            }
        }

        private void toolStripMenuItem1_DropDownOpening(object sender, EventArgs e)
        {
            this.toolStripMenuItem1.DropDownItems[0].Enabled = this.frames.Count > 1;
        }

        private void toolStripMenuItem1_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            var index = toolStripMenuItem1.DropDownItems.IndexOf(e.ClickedItem);
            switch (index)
            {
                case 0:
                    this.DialogResult = DialogResult.OK;
                    this.Hide();
                    break;
                case 4:
                    this.resetFrames();
                    break;
                case 8:
                    this.DialogResult = DialogResult.Cancel;
                    this.Hide();
                    break;
            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            this.blockUnit = true;
            this.toolStripMenuItem3.Checked = true;
            this.toolStripMenuItem4.Checked = false;
            this.toolStripMenuItem5.Checked = true;
            this.toolStripMenuItem6.Checked = false;
            this.cursorLocation.X -= this.cursorLocation.X % this.blockSize.Width;
            this.cursorLocation.Y -= this.cursorLocation.Y % this.blockSize.Height;
            this.pictureBox1.Refresh();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            this.blockUnit = false;
            toolStripMenuItem3.Checked = false;
            toolStripMenuItem4.Checked = true;
            toolStripMenuItem5.Checked = false;
            toolStripMenuItem6.Checked = true;
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            this.toolStripMenuItem8.Enabled = false;
            var form4 = new Form4(this.pictureBox1);
            form4.Text += " for 合体配置";
            form4.Disposed += (s1, e1) => {
                this.toolStripMenuItem8.Enabled = true;
            };
            form4.Show(this);
        }

        private void InitializeComponent()
        {
            this.pictureBox1 = new PictureBox();
            this.panel1 = new Panel();
            this.contextMenuStrip1 = new ContextMenuStrip();
            this.menuStrip1 = new MenuStrip();
            this.toolStripMenuItem1 = new ToolStripMenuItem();
            this.toolStripMenuItem2 = new ToolStripMenuItem();
            this.toolStripMenuItem3 = new ToolStripMenuItem();
            this.toolStripMenuItem4 = new ToolStripMenuItem();
            this.toolStripMenuItem5 = new ToolStripMenuItem();
            this.toolStripMenuItem6 = new ToolStripMenuItem();
            this.toolStripMenuItem7 = new ToolStripMenuItem();
            this.toolStripMenuItem8 = new ToolStripMenuItem();

            this.panel1.SuspendLayout();
            this.SuspendLayout();

            this.toolStripMenuItem5.Text = "ブロック単位(&B)";
            this.toolStripMenuItem5.Checked = true;
            this.toolStripMenuItem5.Click += new EventHandler(this.toolStripMenuItem3_Click);

            this.toolStripMenuItem6.Text = "セル単位(&C)";
            this.toolStripMenuItem6.Click += new EventHandler(this.toolStripMenuItem4_Click);

            this.toolStripMenuItem7.DropDownItems.Add(this.toolStripMenuItem5);
            this.toolStripMenuItem7.DropDownItems.Add(this.toolStripMenuItem6);
            this.toolStripMenuItem7.Text = "位置合わせ(&P)";

            this.contextMenuStrip1.Items.Add("設置(&S)");
            this.contextMenuStrip1.Items.Add("削除(&D)");
            this.contextMenuStrip1.Items.Add("-");
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem7);
            this.contextMenuStrip1.Items.Add("-");
            this.contextMenuStrip1.Items.Add("キャンセル(&C)");
            this.contextMenuStrip1.ItemClicked +=
                new ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);

            this.toolStripMenuItem3.Text = "ブロック単位 (&B)";
            this.toolStripMenuItem3.Checked = true;
            this.toolStripMenuItem3.Click += new EventHandler(this.toolStripMenuItem3_Click);

            this.toolStripMenuItem4.Text = "セル単位(&C)";
            this.toolStripMenuItem4.Click += new EventHandler(this.toolStripMenuItem4_Click);

            this.toolStripMenuItem2.DropDownItems.Add(this.toolStripMenuItem3);
            this.toolStripMenuItem2.DropDownItems.Add(this.toolStripMenuItem4);
            this.toolStripMenuItem2.Text = "位置合わせ(&P)";

            this.toolStripMenuItem8.Text = "補助背景(&H)";
            this.toolStripMenuItem8.Click += new EventHandler(this.toolStripMenuItem8_Click);

            this.toolStripMenuItem1.DropDownItems.Add("配置完了(&A)");
            this.toolStripMenuItem1.DropDownItems.Add("-");
            this.toolStripMenuItem1.DropDownItems.Add(this.toolStripMenuItem2);
            this.toolStripMenuItem1.DropDownItems.Add("-");
            this.toolStripMenuItem1.DropDownItems.Add("リセット(&R)");
            this.toolStripMenuItem1.DropDownItems.Add("-");
            this.toolStripMenuItem1.DropDownItems.Add(this.toolStripMenuItem8);
            this.toolStripMenuItem1.DropDownItems.Add("-");
            this.toolStripMenuItem1.DropDownItems.Add("中止(&C)");
            this.toolStripMenuItem1.Text = "メニュー";
            this.toolStripMenuItem1.DropDownOpening +=
                new EventHandler(this.toolStripMenuItem1_DropDownOpening);
            this.toolStripMenuItem1.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.toolStripMenuItem1_DropDownItemClicked);

            this.menuStrip1.Items.Add(this.toolStripMenuItem1);
            this.menuStrip1.Dock = DockStyle.Top;

            this.pictureBox1.Size = new Size(900, 900);
            this.pictureBox1.BackColor = Color.White;
            this.pictureBox1.Paint += new PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseClick += new MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.pictureBox1.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Top = this.menuStrip1.Bottom;
            this.panel1.Size = new Size(
                this.ClientSize.Width,
                this.ClientSize.Height -
                this.menuStrip1.Height
            );
            this.panel1.AutoScroll = true;
            this.panel1.Anchor =
                AnchorStyles.Left |
                AnchorStyles.Right |
                AnchorStyles.Top |
                AnchorStyles.Bottom;
            this.panel1.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.panel1.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ShowInTaskbar = false;
            this.Text = "配置";
            this.Load += new EventHandler(this.Form2_Load);
            this.Shown += new EventHandler(this.Form2_Shown);
            this.SizeChanged += new EventHandler(this.Form2_SizeChanged);
            this.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private PictureBox pictureBox1;
        private Panel panel1;
        private ContextMenuStrip contextMenuStrip1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripMenuItem toolStripMenuItem3;
        private ToolStripMenuItem toolStripMenuItem4;
        private ToolStripMenuItem toolStripMenuItem5;
        private ToolStripMenuItem toolStripMenuItem6;
        private ToolStripMenuItem toolStripMenuItem7;
        private ToolStripMenuItem toolStripMenuItem8;
    }
}
