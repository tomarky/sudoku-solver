using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;

namespace SudokuSolver
{
    internal sealed class Solver
    {
        internal readonly int[] Numbers;
        internal readonly long[] Groups;

        private List<Cell> emptyCells = new List<Cell>();
        private readonly Problem problem;

        private Solver(Solver source)
        {
            this.problem = source.problem;
            this.Numbers = (int[])source.Numbers.Clone();
            this.Groups = (long[])source.Groups.Clone();
            this.emptyCells = new List<Cell>(source.emptyCells);
        }

        internal Solver(Problem problem)
        {
            this.problem = problem;
            this.Numbers = new int[problem.Cells.Count];
            foreach (var cell in problem.Cells)
            {
                this.Numbers[cell.Index] = cell.Number;
                if (cell.Number == 0)
                {
                    this.emptyCells.Add(cell);
                }
            }
            this.Groups = new long[problem.CellGroups.Count];
            foreach (var g in problem.CellGroups)
            {
                this.Groups[g.Index] = g.FillState();
            }
        }

        private void copyTo(Solver dst)
        {
            this.Numbers.CopyTo(dst.Numbers, 0);
            this.Groups.CopyTo(dst.Groups, 0);
            dst.emptyCells.Clear();
            dst.emptyCells.AddRange(this.emptyCells);
        }

        internal bool IsCompleted
        {
            get
            {
                return this.emptyCells.Count == 0;
            }
        }

        internal bool IsEmptyCell(Cell cell)
        {
            return this.Numbers[cell.Index] == 0;
        }

        internal FillResult Solve(CancellationToken token)
        {
            var count = this.emptyCells.Count;
            if (count == 0)
            {
                return FillResult.NotFilled;
            }
            switch (this.logicalFill(token))
            {
                case FillResult.Filled:
                    if (this.IsCompleted)
                    {
                        return FillResult.Filled;
                    }
                    break;
                case FillResult.Conflict:
                    return FillResult.Conflict;
                case FillResult.Canceled:
                    return FillResult.Canceled;
            }
            while (!token.IsCancellationRequested)
            {
                switch (this.provisionalCellFill(token))
                {
                    case FillResult.Filled:
                        if (this.IsCompleted)
                        {
                            return FillResult.Filled;
                        }
                        break;
                    case FillResult.Conflict:
                        return FillResult.Conflict;
                    case FillResult.Canceled:
                        return FillResult.Canceled;
                }
                var noChange = true;
                switch (this.provisionalGroupFill(token))
                {
                    case FillResult.Filled:
                        if (this.IsCompleted)
                        {
                            return FillResult.Filled;
                        }
                        noChange = false;
                        break;
                    case FillResult.Conflict:
                        return FillResult.Conflict;
                    case FillResult.Canceled:
                        return FillResult.Canceled;
                }
                if (noChange)
                {
                    break;
                }
            }
            if (token.IsCancellationRequested)
            {
                return FillResult.Canceled;
            }
            var ok = false;
            foreach (var g in this.problem.CellGroups)
            {
                var filled = (int)Utils.PopCount(this.Groups[g.Index] >> 1);
                var remain = g.MaxNumber - filled;
                if (remain >= 2 && remain <= 7)
                {
                    ok = true;
                    break;
                }
            }
            if (ok)
            {
                switch (this.provisionalGroupSolve(token))
                {
                    case FillResult.Filled:
                        if (this.IsCompleted)
                        {
                            return FillResult.Filled;
                        }
                        break;
                    case FillResult.Conflict:
                        return FillResult.Conflict;
                    case FillResult.Canceled:
                        return FillResult.Canceled;
                }
            }
            else
            {
                switch (this.provisionalCellSolve(token))
                {
                    case FillResult.Filled:
                        if (this.IsCompleted)
                        {
                            return FillResult.Filled;
                        }
                        break;
                    case FillResult.Conflict:
                        return FillResult.Conflict;
                    case FillResult.Canceled:
                        return FillResult.Canceled;
                }
            }
            return count == this.emptyCells.Count
                ? FillResult.NotFilled
                : FillResult.Filled;
        }

        private FillResult provisionalCellFill(CancellationToken token)
        {
            var count = this.emptyCells.Count;
            if (count == 0)
            {
                return FillResult.NotFilled;
            }
            var valid = false;
            while (!token.IsCancellationRequested)
            {
                this.emptyCells.Sort( (x, y) => {
                    var cx = (int)Utils.PopCount(this.cellFillable(x));
                    var cy = (int)Utils.PopCount(this.cellFillable(y));
                    if (cx == cy)
                    {
                        cx = x.Index;
                        cy = y.Index;
                    }
                    return cx.CompareTo(cy);
                });
                valid = false;
                var noChange = true;
                for (var i = 0; i < this.emptyCells.Count; i++)
                {
                    var cell = this.emptyCells[i];
                    if (!this.IsEmptyCell(cell))
                    {
                        continue;
                    }
                    var flag = this.cellFillable(cell) << 1;
                    if (flag == 0L)
                    {
                        return FillResult.Conflict;
                    }
                    Solver okState = null;
                    var okCount = 0;
                    for (var n = 1; n <= cell.MaxNumber; n++)
                    {
                        if (!Utils.HasBit(flag, n))
                        {
                            continue;
                        }
                        if (token.IsCancellationRequested)
                        {
                            return FillResult.Canceled;
                        }
                        var solver = new Solver(this);
                        solver.fill(cell, n);
                        switch (solver.logicalFill(token))
                        {
                            case FillResult.Filled:
                                if (solver.IsCompleted)
                                {
                                    solver.copyTo(this);
                                    return FillResult.Filled;
                                }
                                okState = solver;
                                okCount++;
                                break;
                            case FillResult.NotFilled:
                                okCount++;
                                break;
                            case FillResult.Canceled:
                                return FillResult.Canceled;
                        }
                    }
                    valid |= okCount > 0;
                    if (okState != null && okCount == 1)
                    {
                        okState.copyTo(this);
                        noChange = false;
                        break;
                    }
                }
                if (noChange)
                {
                    break;
                }
            }
            return token.IsCancellationRequested
                ? FillResult.Canceled
                : !valid
                ? FillResult.Conflict
                : count == this.emptyCells.Count
                ? FillResult.NotFilled
                : FillResult.Filled;
        }

        private FillResult provisionalCellSolve(CancellationToken token)
        {
            var count = this.emptyCells.Count;
            if (count == 0)
            {
                return FillResult.NotFilled;
            }
            this.emptyCells.Sort( (x, y) => {
                var cx = (int)Utils.PopCount(this.cellFillable(x));
                var cy = (int)Utils.PopCount(this.cellFillable(y));
                if (cx == cy)
                {
                    cx = x.Index;
                    cy = y.Index;
                }
                return cx.CompareTo(cy);
            });
            for (var i = 0; i < this.emptyCells.Count; i++)
            {
                var cell = this.emptyCells[i];
                if (!this.IsEmptyCell(cell))
                {
                    continue;
                }
                var flag = this.cellFillable(cell) << 1;
                if (flag == 0L)
                {
                    return FillResult.Conflict;
                }
                for (var n = 1; n <= cell.MaxNumber; n++)
                {
                    if (!Utils.HasBit(flag, n))
                    {
                        continue;
                    }
                    if (token.IsCancellationRequested)
                    {
                        return FillResult.Canceled;
                    }
                    var solver = new Solver(this);
                    solver.fill(cell, n);
                    switch (solver.Solve(token))
                    {
                        case FillResult.Filled:
                            if (solver.IsCompleted)
                            {
                                solver.copyTo(this);
                                return FillResult.Filled;
                            }
                            break;
                        case FillResult.Canceled:
                            return FillResult.Canceled;
                    }
                }
                break;
            }
            return token.IsCancellationRequested
                ? FillResult.Canceled
                : FillResult.NotFilled;
        }

        private FillResult provisionalGroupFill(CancellationToken token)
        {
            var count = this.emptyCells.Count;
            if (count == 0)
            {
                return FillResult.NotFilled;
            }
            var groups = new List<CellGroup>(this.problem.CellGroups);
            var cells = new List<Cell>(7);
            var fillables = new List<long>(7);
            var nums = new List<int>(7);
            var okPerms = new List<int[]>(7*6*5*4*3*2*1);
            while (!token.IsCancellationRequested)
            {
                groups.Sort( (x, y) => {
                    var cx = (int)Utils.PopCount(this.Groups[x.Index]);
                    var cy = (int)Utils.PopCount(this.Groups[y.Index]);
                    if (cx == cy)
                    {
                        cx = -Math.Min(x.CellBounds.Width, x.CellBounds.Height);
                        cy = -Math.Min(y.CellBounds.Width, y.CellBounds.Height);
                        if (cx == cy)
                        {
                            cx = -x.Index;
                            cy = -y.Index;
                        }
                    }
                    return cy.CompareTo(cx);
                });
                var noChange = true;
                foreach (var g in groups)
                {
                    var filled = (int)Utils.PopCount(this.Groups[g.Index] >> 1);
                    var remain = g.MaxNumber - filled;
                    if (remain == 1)
                    {
                        return FillResult.Conflict;
                    }
                    else if (remain == 0 || remain > 6)
                    {
                        continue;
                    }
                    if (token.IsCancellationRequested)
                    {
                        return FillResult.Canceled;
                    }
                    cells.Clear();
                    fillables.Clear();
                    foreach (var c in g.Cells)
                    {
                        if (this.IsEmptyCell(c))
                        {
                            cells.Add(c);
                            fillables.Add(this.cellFillable(c) << 1);
                        }
                    }
                    nums.Clear();
                    for (var n = 1; n <= g.MaxNumber; n++)
                    {
                        if (!Utils.HasBit(this.Groups[g.Index], n))
                        {
                            nums.Add(n);
                        }
                    }
                    okPerms.Clear();
                    Solver okState = null;
                    var perms = Utils.GetPermutation(remain);
                    foreach (var perm in perms)
                    {
                        var applicable = true;
                        for (var i = 0; i < remain; i++)
                        {
                            var n = nums[perm[i]];
                            if (!Utils.HasBit(fillables[i], n))
                            {
                                applicable = false;
                                break;
                            }
                        }
                        if (!applicable)
                        {
                            continue;
                        }
                        if (token.IsCancellationRequested)
                        {
                            return FillResult.Canceled;
                        }
                        var solver = new Solver(this);
                        for (var i = 0; i < remain; i++)
                        {
                            var n = nums[perm[i]];
                            solver.fill(cells[i], n);
                        }
                        switch (solver.logicalFill(token))
                        {
                            case FillResult.Filled:
                                if (solver.IsCompleted)
                                {
                                    solver.copyTo(this);
                                    return FillResult.Filled;
                                }
                                okPerms.Add(perm);
                                okState = solver;
                                break;
                            case FillResult.NotFilled:
                                okPerms.Add(perm);
                                okState = solver;
                                break;
                            case FillResult.Canceled:
                                return FillResult.Canceled;
                        }
                    }
                    if (okPerms.Count == 0)
                    {
                        return FillResult.Conflict;
                    }
                    else if (okPerms.Count == 1 && okState != null)
                    {
                        okState.copyTo(this);
                        noChange = false;
                    }
                    else
                    {
                        var array = (int[])okPerms[0].Clone();
                        foreach (var perm in okPerms)
                        {
                            for (var i = 0; i < array.Length; i++)
                            {
                                if (array[i] != perm[i])
                                {
                                    array[i] = -1;
                                }
                            }
                        }
                        var ok = false;
                        for (var i = 0; i < array.Length; i++)
                        {
                            if (array[i] >= 0)
                            {
                                var n = nums[array[i]];
                                this.fill(cells[i], n);
                                ok = true;
                            }
                        }
                        if (ok)
                        {
                            noChange = false;
                            switch (this.logicalFill(token))
                            {
                                case FillResult.Filled:
                                    if (this.IsCompleted)
                                    {
                                        return FillResult.Filled;
                                    }
                                    break;
                                case FillResult.Conflict:
                                    return FillResult.Conflict;
                                case FillResult.Canceled:
                                    return FillResult.Canceled;
                            }
                        }
                    }
                }
                if (noChange)
                {
                    break;
                }
            }
            return token.IsCancellationRequested
                ? FillResult.Canceled
                : count == this.emptyCells.Count
                ? FillResult.NotFilled
                : FillResult.Filled;
        }

        private FillResult provisionalGroupSolve(CancellationToken token)
        {
            var count = this.emptyCells.Count;
            if (count == 0)
            {
                return FillResult.NotFilled;
            }
            var groups = new List<CellGroup>(this.problem.CellGroups);
            groups.Sort( (x, y) => {
                var cx = (int)Utils.PopCount(this.Groups[x.Index]);
                var cy = (int)Utils.PopCount(this.Groups[y.Index]);
                if (cx == cy)
                {
                    cx = -Math.Min(x.CellBounds.Width, x.CellBounds.Height);
                    cy = -Math.Min(y.CellBounds.Width, y.CellBounds.Height);
                    if (cx == cy)
                    {
                        cx = -x.Index;
                        cy = -y.Index;
                    }
                }
                return cy.CompareTo(cx);
            });
            foreach (var g in groups)
            {
                var filled = (int)Utils.PopCount(this.Groups[g.Index] >> 1);
                var remain = g.MaxNumber - filled;
                if (remain == 1)
                {
                    return FillResult.Conflict;
                }
                else if (remain == 0 || remain > 7)
                {
                    continue;
                }
                if (token.IsCancellationRequested)
                {
                    return FillResult.Canceled;
                }
                var cells = new List<Cell>(remain);
                var fillables = new List<long>(remain);
                foreach (var c in g.Cells)
                {
                    if (this.IsEmptyCell(c))
                    {
                        cells.Add(c);
                        fillables.Add(this.cellFillable(c) << 1);
                    }
                }
                var nums = new List<int>(remain);
                for (var n = 1; n <= g.MaxNumber; n++)
                {
                    if (!Utils.HasBit(this.Groups[g.Index], n))
                    {
                        nums.Add(n);
                    }
                }
                var perms = Utils.GetPermutation(remain);
                foreach (var perm in perms)
                {
                    var applicable = true;
                    for (var i = 0; i < remain; i++)
                    {
                        var n = nums[perm[i]];
                        if (!Utils.HasBit(fillables[i], n))
                        {
                            applicable = false;
                            break;
                        }
                    }
                    if (!applicable)
                    {
                        continue;
                    }
                    if (token.IsCancellationRequested)
                    {
                        return FillResult.Canceled;
                    }
                    var solver = new Solver(this);
                    for (var i = 0; i < remain; i++)
                    {
                        var n = nums[perm[i]];
                        solver.fill(cells[i], n);
                    }
                    switch (solver.Solve(token))
                    {
                        case FillResult.Filled:
                            if (solver.IsCompleted)
                            {
                                solver.copyTo(this);
                                return FillResult.Filled;
                            }
                            break;
                        case FillResult.Canceled:
                            return FillResult.Canceled;
                    }
                }
                break;
            }
            return token.IsCancellationRequested
                ? FillResult.Canceled
                : FillResult.NotFilled;
        }

        private FillResult logicalFill(CancellationToken token)
        {
            var count = this.emptyCells.Count;
            while (!token.IsCancellationRequested)
            {
                var noChange = true;
                for (var i = 0; i < this.emptyCells.Count; i++)
                {
                    var cell = this.emptyCells[i];
                    if (!this.IsEmptyCell(cell))
                    {
                        Utils.SwapRemove(this.emptyCells, i);
                        i--;
                        continue;
                    }
                    switch (this.tryFill(cell))
                    {
                        case FillResult.Filled:
                            Utils.SwapRemove(this.emptyCells, i);
                            i--;
                            noChange = false;
                            break;
                        case FillResult.Conflict:
                            return FillResult.Conflict;
                    }
                }
                if (noChange)
                {
                    if (this.IsCompleted)
                    {
                        return count > 0
                            ? FillResult.Filled
                            : FillResult.NotFilled;
                    }
                    break;
                }
            }
            foreach (var g in this.problem.CellGroups)
            {
                if (token.IsCancellationRequested)
                {
                    return FillResult.Canceled;
                }
                var flag = 0L;
                foreach (var c in g.Cells)
                {
                    if (this.IsEmptyCell(c))
                    {
                        flag |= this.cellFillable(c);
                    }
                }
                flag ^= this.Groups[g.Index] >> 1;
                if (flag != (1L << g.MaxNumber) - 1L)
                {
                    return FillResult.Conflict;
                }
            }
            return count == this.emptyCells.Count
                ? FillResult.NotFilled
                : FillResult.Filled;
        }

        private void fill(Cell cell, int number)
        {
            this.Numbers[cell.Index] = number;
            foreach (var g in cell.Groups)
            {
                this.Groups[g.Index] |= 1L << number;
            }
        }

        private long cellFillable(Cell cell)
        {
            long flag = 0L;
            foreach (var g in cell.Groups)
            {
                flag |= this.Groups[g.Index];
            }
            flag >>= 1;
            flag ^= (1L << cell.MaxNumber) - 1L;
            return flag;
        }

        private FillResult tryFill(Cell cell)
        {
            var flag = this.cellFillable(cell);
            if (flag == 0L)
            {
                return FillResult.Conflict;
            }
            if (Utils.IsSingleBit(flag))
            {
                var number = Utils.SingleBitLog2(flag);
                this.fill(cell, number);
                return FillResult.Filled;
            }
            foreach (var g in cell.Groups)
            {
                var flag2 = flag;
                foreach (var c in g.Cells)
                {
                    if (c.Index == cell.Index || !this.IsEmptyCell(c))
                    {
                        continue;
                    }
                    flag2 &= ~this.cellFillable(c);
                }
                if (Utils.IsSingleBit(flag2))
                {
                    var number = Utils.SingleBitLog2(flag2);
                    this.fill(cell, number);
                    return FillResult.Filled;
                }
            }
            return FillResult.NotFilled;
        }
    }
}