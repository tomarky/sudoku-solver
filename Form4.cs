using System;
using System.Drawing;
using System.Windows.Forms;

namespace SudokuSolver
{
    internal sealed class Form4 : Form
    {
        private const string FileFilter =
            "画像ファイル|*.jpg;*.jpeg;*.png;*.bmp;*.gif|All Files|*.*";

        private PictureBox targetPictureBox = null;

        internal Form4(PictureBox targetPictureBox)
        {
            this.targetPictureBox = targetPictureBox;
            this.InitializeComponent();
        }

        internal bool ButtonEnabled
        {
            get
            {
                return this.button1.Enabled;
            }
            set
            {
                this.setButtonsEnable(value);
            }
        }

        private void setButtonsEnable(bool newValue)
        {
            if (this.button1.InvokeRequired)
            {
                this.button1.Invoke(
                    new Action<bool>(this.setButtonsEnable),
                    new object[] { newValue }
                );
            }
            else
            {
                this.button1.Enabled = newValue;
                this.button3.Enabled = newValue;
            }
        }

        private class Setting
        {
            internal string FilePath = "";
            internal float OffSetX = 0;
            internal float OffSetY = 0;
            internal float Scale = 0;
            internal float Rotation = 0;
        }

        private void refreshTarget(Form4.Setting setting)
        {
            if (this.targetPictureBox.InvokeRequired)
            {
                this.targetPictureBox.Invoke(
                    new Action<Form4.Setting>(this.refreshTarget),
                    new object[] { setting }
                );
                return;
            }
            if (setting.FilePath == "")
            {
                this.targetPictureBox.Image = null;
                this.targetPictureBox.Refresh();
                return;
            }
            var canvas = this.targetPictureBox.Image;
            if (canvas == null)
            {
                canvas = new Bitmap(
                    this.targetPictureBox.ClientSize.Width,
                    this.targetPictureBox.ClientSize.Height
                );
            }
            try
            {
                using (var image = Image.FromFile(setting.FilePath))
                {
                    using (var g = Graphics.FromImage(canvas))
                    {
                        g.Clear(Color.White);
                        g.RotateTransform(setting.Rotation);
                        g.ScaleTransform(
                            setting.Scale,
                            setting.Scale
                        );
                        g.DrawImage(
                            image,
                            setting.OffSetX,
                            setting.OffSetY
                        );
                    }
                }
            }
            catch
            {
                MessageBox.Show("画像の読み込みに失敗しました", Program.Name);
                return;
            }
            this.targetPictureBox.Image = canvas;
            this.targetPictureBox.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.refreshTarget(new Form4.Setting() {
                FilePath = this.textBox1.Text,
                OffSetX = (float)this.numericUpDown1.Value,
                OffSetY = (float)this.numericUpDown2.Value,
                Scale = (float)this.numericUpDown3.Value,
                Rotation = (float)this.numericUpDown4.Value
            });
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string fileName = "";
            using (var dialog = new OpenFileDialog())
            {
                dialog.Filter = Form4.FileFilter;
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                fileName = dialog.FileName;
            }
            this.textBox1.Text = fileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = "";
            this.numericUpDown1.Value = 0;
            this.numericUpDown2.Value = 0;
            this.numericUpDown3.Value = 1;
            this.numericUpDown4.Value = 0;
            this.refreshTarget(new Form4.Setting());
        }

        private void Form4_Activated(object sender, EventArgs e)
        {
            this.Opacity = 1.0;
        }

        private void Form4_Deactivate(object sender, EventArgs e)
        {
            this.Opacity = 0.75;
        }

        private void InitializeComponent()
        {
            this.button1 = new Button();
            this.button2 = new Button();
            this.button3 = new Button();
            this.label1 = new Label();
            this.label2 = new Label();
            this.label3 = new Label();
            this.label4 = new Label();
            this.label5 = new Label();
            this.numericUpDown1 = new NumericUpDown();
            this.numericUpDown2 = new NumericUpDown();
            this.numericUpDown3 = new NumericUpDown();
            this.numericUpDown4 = new NumericUpDown();
            this.textBox1 = new TextBox();

            this.button1.Text = "設定反映";
            this.button1.Click += new EventHandler(this.button1_Click);
            this.Controls.Add(this.button1);

            this.button3.Left = this.button1.Bounds.Right + 5;
            this.button3.Text = "リセット";
            this.button3.Click += new EventHandler(this.button3_Click);
            this.Controls.Add(this.button3);

            this.label1.Top = this.button1.Bounds.Bottom + 5;
            this.label1.AutoSize = true;
            this.label1.Text = "画像ファイル:";
            this.Controls.Add(this.label1);

            this.textBox1.Location = new Point(
                15,
                this.label1.Bounds.Bottom
            );
            this.textBox1.Width = 250;
            this.Controls.Add(this.textBox1);

            this.button2.Location = new Point(
                this.textBox1.Bounds.Right,
                this.label1.Bounds.Bottom
            );
            this.button2.AutoSize = true;
            this.button2.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.button2.Text = "...";
            this.button2.Click += new EventHandler(this.button2_Click);
            this.Controls.Add(this.button2);

            this.label2.Top = Math.Max(
                this.button2.Bounds.Bottom,
                this.textBox1.Bounds.Bottom
            ) + 5;
            this.label2.AutoSize = true;
            this.label2.Text = "横方向オフセット値: (-5000 - 5000)";
            this.Controls.Add(this.label2);

            this.numericUpDown1.Location = new Point(
                15,
                this.label2.Bounds.Bottom
            );
            this.numericUpDown1.Value = 0;
            this.numericUpDown1.Maximum = 5000;
            this.numericUpDown1.Minimum = -5000;
            this.Controls.Add(this.numericUpDown1);

            this.label3.Top = this.numericUpDown1.Bounds.Bottom + 5;
            this.label3.AutoSize = true;
            this.label3.Text = "縦方向オフセット値: (-5000 - 5000)";
            this.Controls.Add(this.label3);

            this.numericUpDown2.Location = new Point(
                15,
                this.label3.Bounds.Bottom
            );
            this.numericUpDown2.Value = 0;
            this.numericUpDown2.Maximum = 5000;
            this.numericUpDown2.Minimum = -5000;
            this.Controls.Add(this.numericUpDown2);

            this.label4.Top = this.numericUpDown2.Bounds.Bottom + 5;
            this.label4.AutoSize = true;
            this.label4.Text = "拡大縮小倍率: (0.01 - 50.00)";
            this.Controls.Add(this.label4);

            this.numericUpDown3.Location = new Point(
                15,
                this.label4.Bounds.Bottom
            );
            this.numericUpDown3.Value = 1;
            this.numericUpDown3.DecimalPlaces = 2;
            this.numericUpDown3.Increment = 0.01M;
            this.numericUpDown3.Maximum = 50;
            this.numericUpDown3.Minimum = 0.01M;
            this.Controls.Add(this.numericUpDown3);

            this.label5.Top = this.numericUpDown3.Bounds.Bottom + 5;
            this.label5.AutoSize = true;
            this.label5.Text = "回転: (-360.0 - 360.0)";
            this.Controls.Add(this.label5);

            this.numericUpDown4.Location = new Point(
                15,
                this.label5.Bounds.Bottom
            );
            this.numericUpDown4.Value = 0;
            this.numericUpDown4.DecimalPlaces = 1;
            this.numericUpDown4.Increment = 0.1M;
            this.numericUpDown4.Maximum = 360;
            this.numericUpDown4.Minimum = -360;
            this.Controls.Add(this.numericUpDown4);

            this.Size = new Size(
                this.button2.Bounds.Right + 5
                    + this.Size.Width - this.ClientSize.Width,
                this.numericUpDown4.Bounds.Bottom + 5
                    + this.Size.Height - this.ClientSize.Height
            );
            this.MinimizeBox = false;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ShowInTaskbar = false;
            this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            this.Activated += new EventHandler(this.Form4_Activated);
            this.Deactivate += new EventHandler(this.Form4_Deactivate);
            this.Text = "補助背景";
        }

        private Button button1;
        private Button button2;
        private Button button3;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label5;
        private NumericUpDown numericUpDown1;
        private NumericUpDown numericUpDown2;
        private NumericUpDown numericUpDown3;
        private NumericUpDown numericUpDown4;
        private TextBox textBox1;
    }
}