using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SudokuSolver
{
    internal sealed class WRList<T> : IList<T>
        where T : class
    {
        private readonly List<WeakReference<T>> list;

        internal WRList()
        {
            this.list = new List<WeakReference<T>>();
        }

        internal WRList(int capacity)
        {
            this.list = new List<WeakReference<T>>(capacity);
        }

        public int IndexOf(T item)
        {
            for (var i = 0; i < this.list.Count; i++)
            {
                T target;
                this.list[i].TryGetTarget(out target);
                if (item == target)
                {
                    return i;
                }
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            this.list.Insert(index, new WeakReference<T>(item));
        }

        public void RemoveAt(int index)
        {
            this.list.RemoveAt(index);
        }

        public T this[int i]
        {
            get
            {
                T target;
                this.list[i].TryGetTarget(out target);
                return target;
            }
            set
            {
                this.list[i].SetTarget(value);
            }
        }

        public void Add(T item)
        {
            this.list.Add(new WeakReference<T>(item));
        }

        public void Clear()
        {
            this.list.Clear();
        }

        public bool Contains(T item)
        {
            return this.IndexOf(item) >= 0;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException();
            }
            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (array.Length - arrayIndex < this.list.Count)
            {
                throw new ArgumentException();
            }
            foreach (var wr in this.list)
            {
                wr.TryGetTarget(out array[arrayIndex]);
                arrayIndex++;
            }
        }

        public bool Remove(T item)
        {
            var index = this.IndexOf(item);
            if (index < 0)
            {
                return false;
            }
            this.RemoveAt(index);
            return true;
        }

        public int Count
        {
            get
            {
                return this.list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public IList<T> AsReadOnly()
        {
            return new ReadOnlyCollection<T>(this);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new WREnumerator(this.list.GetEnumerator());
        }

        private System.Collections.IEnumerator GetEnumerator1()
        {
            return this.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator1();
        }

        private sealed class WREnumerator : IEnumerator<T>
        {
            private readonly IEnumerator<WeakReference<T>> enumerator;

            private T _current = null;

            internal WREnumerator(IEnumerator<WeakReference<T>> enumerator)
            {
                this.enumerator = enumerator;
            }

            public T Current
            {
                get
                {
                    return this._current;
                }
            }

            object System.Collections.IEnumerator.Current
            {
                get
                {
                    return this._current;
                }
            }

            public bool MoveNext()
            {
                if (this.enumerator.MoveNext())
                {
                    this.enumerator.Current.TryGetTarget(out this._current);
                    return true;
                }
                else
                {
                    this._current = null;
                    return false;
                }
            }

            public void Dispose()
            {
                this.enumerator.Dispose();
            }

            public void Reset()
            {
                this.enumerator.Reset();
            }
        }
    }
}