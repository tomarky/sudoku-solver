数独そるばー



開発環境
 Windows 7 SP1 (32bit)
 .NET Framework 4.8
 コンパイル言語 C# 5




ビルドコマンド
 .NET Framework 4.6 ～ 4.8 で C#コンパイラ csc.exe を直接使用する場合
 
csc.exe /out:SudokuSolver.exe /target:winexe /platform:anycpu /debug- /optimize+ /warnaserror+ /checked- /codepage:65001 /main:SudokuSolver.Program /nologo *.cs



ビルドコマンド
 .NET CLI (.NET SDK) を使用する場合

dotnet build -c Release

