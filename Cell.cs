using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SudokuSolver
{
    internal sealed class Cell
    {
        internal const int CellSize = Form1.CellSize;

        internal static readonly string[] Numbers = {
            "0", "1", "2", "3", "4",
            "5", "6", "7", "8", "9",
            "10", "11", "12", "13", "14",
            "15", "16", "17", "18", "19",
            "20", "21", "22", "23", "24",
            "25", "26", "27", "28", "29",
            "30", "31", "32", "33", "34",
            "35", "36", "37", "38", "39"
        };

        internal static readonly Font font = new Font(
            new FontFamily("Arial"),
            14,
            FontStyle.Regular
        );

        internal static readonly StringFormat stringFormat =
            new StringFormat(
                StringFormatFlags.NoClip |
                StringFormatFlags.NoWrap
            ) {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Far,
                Trimming = StringTrimming.None
            };

        static readonly Pen FixedPen = new Pen(
            Color.FromArgb(0x80, Color.Gray), 4
        ) {
            Alignment = PenAlignment.Inset
        };

        internal readonly int Index;
        internal readonly Point CellLocation;

        readonly RectangleF rectangle;

        internal IList<CellGroup> Groups { get; private set; }
        internal int MaxNumber { get; private set; }

        internal bool Fixed = false;
        internal bool Marked = false;
        internal int Invalid = 0;

        private int _number = 0;

        internal Cell(int index, Point cellLocation)
        {
            this.Index = index;
            this.CellLocation = cellLocation;
            this.rectangle = new RectangleF(
                cellLocation.X * Cell.CellSize,
                cellLocation.Y * Cell.CellSize,
                Cell.CellSize,
                Cell.CellSize
            );
        }

        internal int Number
        {
            get
            {
                return this._number;
            }
            set
            {
                if (this._number == value)
                {
                    return;
                }
                var oldNumber = this._number;
                this._number = 0;
                this.Invalid = 0;
                foreach (var g in this.Groups)
                {
                    this.Invalid += g.Verify(oldNumber, value);
                }
                this._number = value;
            }
        }

        internal bool IsValid
        {
            get
            {
                return this.Invalid == 0;
            }
        }

        internal bool IsEmpty
        {
            get
            {
                return this._number == 0;
            }
        }

        internal void AddGroup(CellGroup cellGroup)
        {
            if (this.Groups == null)
            {
                this.Groups = new WRList<CellGroup>();
            }
            this.Groups.Add(cellGroup);
            if (this.MaxNumber == 0 || this.MaxNumber > cellGroup.MaxNumber)
            {
                this.MaxNumber = cellGroup.MaxNumber;
            }
        }

        internal void DrawBackground(Graphics g)
        {
            if (this.Fixed)
            {
                g.DrawRectangle(
                    Cell.FixedPen,
                    this.rectangle.X,
                    this.rectangle.Y,
                    this.rectangle.Width + 1,
                    this.rectangle.Height + 1
                );
            }
        }

        internal void Draw(Graphics g)
        {
            this.Draw(g, this._number);
        }

        internal void Draw(Graphics g, int number)
        {
            if (this.Marked)
            {
                g.DrawRectangle(
                    Pens.Black,
                    this.rectangle.X + 3,
                    this.rectangle.Y + 3,
                    Cell.CellSize - 6,
                    Cell.CellSize - 6
                );
            }
            if (this._number > 0)
            {
                number = this._number;
            }
            if (number == 0)
            {
                return;
            }
            for (var y = -1; y <= 1; y++)
            {
                for (var x = -1; x <= 1; x++)
                {
                    if (x == 0 && y == 0)
                    {
                        continue;
                    }
                    var rect = this.rectangle;
                    rect.Offset(x, y);
                    g.DrawString(
                        Cell.Numbers[number],
                        font,
                        Brushes.White,
                        rect,
                        stringFormat
                    );
                }
            }
            g.DrawString(
                Cell.Numbers[number],
                font,
                this.Invalid > 0
                    ? Brushes.Red
                    : this._number > 0
                    ? Brushes.DarkSlateGray
                    : Brushes.Black,
                this.rectangle,
                stringFormat
            );
        }
    }

}