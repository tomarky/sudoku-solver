using System;
using System.Collections.Generic;

namespace SudokuSolver
{
    internal sealed class Utils
    {
        // Hamming weight
        internal static long PopCount(long n)
        {
            n = (n & 0x5555555555555555L)
              + ((n >> 1) & 0x5555555555555555L);
            n = (n & 0x3333333333333333L)
              + ((n >> 2) & 0x3333333333333333L);
            n = (n & 0x0F0F0F0F0F0F0F0FL)
              + ((n >> 4) & 0x0F0F0F0F0F0F0F0FL);
            n += n >> 8;
            n += n >> 16;
            n += n >> 32;
            return n & 0xFFL;
        }

        internal static bool HasBit(long n, int i)
        {
            return (n & (1L << i)) != 0L;
        }

        internal static bool IsSingleBit(long n)
        {
            return n != 0 && (n & -n) == n;
        }

        internal static int SingleBitLog2(long n)
        {
            var ret = 0;
            while (n > 0L)
            {
                ret++;
                n >>= 1;
            }
            return ret;
        }

        internal static void SwapRemove<T>(IList<T> list, int index)
        {
            var last = list.Count - 1;
            list[index] = list[last];
            list.RemoveAt(last);
        }

        internal static void Swap<T>(T[] list, int index1, int index2)
        {
            T tmp = list[index1];
            list[index1] = list[index2];
            list[index2] = tmp;
        }

        internal static bool NextPermutation(int[] list)
        {
            for (var i = list.Length - 1; i > 0; i--)
            {
                if (list[i - 1] >= list[i])
                {
                    continue;
                }
                var a = i;
                var b = list.Length - 1;
                while (a < b)
                {
                    Utils.Swap(list, a, b);
                    a++;
                    b--;
                }
                for (var p = i; p < list.Length; p++)
                {
                    if (list[p] <= list[i - 1])
                    {
                        continue;
                    }
                    while (p + 1 < list.Length && list[p] == list[p + 1])
                    {
                        p++;
                    }
                    Utils.Swap(list, p, i - 1);
                    return true;
                }
            }
            return false;
        }

        private static IList<int[]>[] permutations = new IList<int[]>[8];

        internal static IList<int[]> GetPermutation(int size)
        {
            if (size < 2 || size > 7)
            {
                throw new ArgumentOutOfRangeException();
            }
            var ret = Utils.permutations[size];
            if (ret != null)
            {
                return ret;
            }
            var array = new int[size];
            var count = 1;
            for (var i = 0; i < size; i++)
            {
                array[i] = i;
                count *= i + 1;
            }
            var list = new List<int[]>(count);
            do
            {
                list.Add((int[])array.Clone());
            } while (Utils.NextPermutation(array));
            ret = list.AsReadOnly();
            Utils.permutations[size] = ret;
            return ret;
        }
    }
}
