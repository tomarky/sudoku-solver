
namespace SudokuSolver
{
    internal enum FillResult
    {
        Filled,
        NotFilled,
        Conflict,
        Canceled
    }
}