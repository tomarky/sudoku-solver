using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace SudokuSolver
{
    [DataContract]
    public sealed class Package
    {
        public Package.Cell[] Cells = null;
        public Package.Frame[] Frames = null;

        [DataContract]
        public sealed class Cell
        {
            public bool Fixed = false;
            public bool Marked = false;
            public int Number = 0;
            public int Index = 0;

            public Cell() {}

            internal Cell(SudokuSolver.Cell cell)
            {
                this.Fixed = cell.Fixed;
                this.Marked = cell.Marked;
                this.Number = cell.Number;
                this.Index = cell.Index;
            }
        }

        [DataContract]
        public sealed class Frame
        {
            public SudokuType Type = SudokuType.Normal;
            public int MaxNumber = 0;
            public Point Location = Point.Empty;
            public Size BlockSize = Size.Empty;
            public int[] FreeStyleGroup = null;

            public Frame() {}

            internal Frame(SudokuFrame frame)
            {
                this.Type = frame.Type;
                this.MaxNumber = frame.MaxNumber;
                this.Location = frame.CellBounds.Location;
                this.BlockSize = frame.BlockSize;
                this.FreeStyleGroup = frame.FreeStyleGroup;
            }
        }

        public Package() {}

        Package(Problem problem)
        {
            var list = new List<Package.Cell>(problem.Cells.Count);
            for (var i = 0; i < problem.Cells.Count; i++)
            {
                var cell = problem.Cells[i];
                if (cell.Fixed || cell.Marked || cell.Number != 0)
                {
                    list.Add(new Package.Cell(problem.Cells[i]));
                }
            }
            this.Cells = list.ToArray();
            this.Frames = new Package.Frame[problem.Frames.Count];
            for (var i = 0; i < this.Frames.Length; i++)
            {
                this.Frames[i] = new Package.Frame(problem.Frames[i]);
            }
        }

        internal static void Save(string fileName, Problem problem)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            var pack = new Package(problem);
            var ser = new XmlSerializer(typeof(Package));
            using (var file = File.Create(fileName))
            {
                ser.Serialize(file, pack);
            }
        }

        internal static Problem Load(string fileName)
        {
            Package pack = null;
            var ser = new XmlSerializer(typeof(Package));
            using (var file = File.OpenRead(fileName))
            {
                pack = (Package)ser.Deserialize(file);
            }
            var problem = new Problem();
            foreach (var f in pack.Frames)
            {
                if (f.Type == SudokuType.FreeStyle)
                {
                    problem.AddFrame(
                        new SudokuFrame(
                            f.Location,
                            f.MaxNumber,
                            f.FreeStyleGroup
                        )
                    );
                }
                else
                {
                    problem.AddFrame(
                        new SudokuFrame(
                            f.Type,
                            f.Location,
                            f.BlockSize
                        )
                    );
                }
            }
            problem.SetUp();
            foreach (var c in pack.Cells)
            {
                var cell = problem.Cells[c.Index];
                cell.Fixed = c.Fixed;
                cell.Marked = c.Marked;
                cell.Number = c.Number;
            }
            return problem;
        }
    }
}