using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace SudokuSolver
{
    internal sealed class SudokuFrame
    {
        internal const int CellSize = Form1.CellSize;

        static readonly Pen Black2Pen = new Pen(Color.Black, 2) {
            Alignment = PenAlignment.Center
        };

        static readonly Pen Black3Pen = new Pen(Color.Black, 3) {
            Alignment = PenAlignment.Center
        };

        static readonly Pen DiagonalPen = new Pen(Color.Gray, 2) {
            Alignment = PenAlignment.Center,
            DashStyle = DashStyle.Dot
        };

        internal static readonly Brush[] CellBrushes = {
            Brushes.LightSteelBlue,
            Brushes.Khaki,
            Brushes.PaleTurquoise,
            Brushes.PeachPuff,
            Brushes.Lavender,
            // Brushes.NavajoWhite,

            Brushes.LightSalmon,
            Brushes.PaleGreen,
            Brushes.LightPink,
            Brushes.Thistle,
            Brushes.SandyBrown,

            Brushes.SkyBlue,
            Brushes.LightCoral,
            Brushes.LightCyan,
            Brushes.Tan,
            Brushes.LightBlue,

            Brushes.Pink,
            Brushes.LightGreen,
            Brushes.Wheat,
            Brushes.PowderBlue,
            Brushes.Salmon,

            Brushes.Gainsboro,
            Brushes.Plum,
            Brushes.PaleGoldenrod,
            Brushes.AntiqueWhite,
            Brushes.LightSkyBlue
        };

        internal readonly Rectangle CellBounds;
        internal readonly Rectangle Bounds;
        internal readonly Size BlockSize;
        internal readonly int MaxNumber;
        internal readonly SudokuType Type;
        internal readonly int[] FreeStyleGroup;

        internal IList<Cell> Cells { get; private set; }
        internal IList<CellGroup> CellGroups { get; private set; }

        private bool hasSetUp = false;

        internal SudokuFrame(int maxNumber) : this(maxNumber, new int[maxNumber * maxNumber]) {}

        internal SudokuFrame(int maxNumber, int[] freeStyleGroup) : this(new Point(0, 0), maxNumber, freeStyleGroup) {}

        internal SudokuFrame(Point location, int maxNumber, int[] freeStyleGroup)
        {
            this.FreeStyleGroup = freeStyleGroup;
            this.Type = SudokuType.FreeStyle;
            this.BlockSize = new Size(1, 1);
            this.MaxNumber = maxNumber;
            this.CellBounds = new Rectangle(
                location.X,
                location.Y,
                maxNumber,
                maxNumber
            );
            this.Bounds = new Rectangle(
                location.X * SudokuFrame.CellSize,
                location.Y * SudokuFrame.CellSize,
                maxNumber * SudokuFrame.CellSize,
                maxNumber * SudokuFrame.CellSize
            );
            this.Cells = new WRList<Cell>();
        }

        internal SudokuFrame(Size blockSize) : this(new Point(0, 0), blockSize) {}

        internal SudokuFrame(Point location, Size blockSize) : this(SudokuType.Normal, location, blockSize) {}

        internal SudokuFrame(SudokuType sudokuType, Size blockSize) : this(sudokuType, new Point(0, 0), blockSize) {}

        internal SudokuFrame(SudokuType sudokuType, Point location, Size blockSize)
        {
            this.FreeStyleGroup = null;
            this.Type = sudokuType;
            this.BlockSize = blockSize;
            this.MaxNumber = blockSize.Width * blockSize.Height;
            this.CellBounds = new Rectangle(
                location.X,
                location.Y,
                this.MaxNumber,
                this.MaxNumber
            );
            this.Bounds = new Rectangle(
                location.X * SudokuFrame.CellSize,
                location.Y * SudokuFrame.CellSize,
                this.MaxNumber * SudokuFrame.CellSize,
                this.MaxNumber * SudokuFrame.CellSize
            );
            this.Cells = new WRList<Cell>();
        }

        internal void SetUp()
        {
            if (this.hasSetUp)
            {
                throw new Exception("has set up");
            }
            this.Cells = ((WRList<Cell>)this.Cells).AsReadOnly();
            var groups = new WRList<CellGroup>();
            for (var a = 0; a < this.MaxNumber; a++)
            {
                var cellGroup1 = CellGroup.GetCellGroup(new Rectangle(
                    this.CellBounds.X,
                    this.CellBounds.Y + a,
                    this.MaxNumber, 1
                ));
                var cellGroup2 = CellGroup.GetCellGroup(new Rectangle(
                    this.CellBounds.X + a,
                    this.CellBounds.Y,
                    1, this.MaxNumber
                ));
                groups.Add(cellGroup1);
                groups.Add(cellGroup2);
                for (var b = 0; b < this.MaxNumber; b++)
                {
                    cellGroup1.AddCell(this.Cells[a + b * this.MaxNumber]);
                    cellGroup2.AddCell(this.Cells[b + a * this.MaxNumber]);
                }
                if (this.Type != SudokuType.FreeStyle)
                {
                    var x = a % this.BlockSize.Width * this.BlockSize.Width;
                    var y = a / this.BlockSize.Width * this.BlockSize.Height;
                    var cellGroup3 = CellGroup.GetCellGroup(new Rectangle(
                        this.CellBounds.X + x,
                        this.CellBounds.Y + y,
                        this.BlockSize.Width,
                        this.BlockSize.Height
                    ));
                    groups.Add(cellGroup3);
                    for (var r = 0; r < this.BlockSize.Height; r++)
                    {
                        for (var c = 0; c < this.BlockSize.Width; c++)
                        {
                            var index = (c + x) + (r + y) * this.MaxNumber;
                            cellGroup3.AddCell(this.Cells[index]);
                        }
                    }
                }
                if (this.Type == SudokuType.Colored)
                {
                    var cellGroup4 = CellGroup.GetCellGroup(
                        CellGroup.Type.Colored,
                        new Rectangle(
                            this.CellBounds.X + a % this.BlockSize.Width,
                            this.CellBounds.Y + a / this.BlockSize.Width,
                            this.MaxNumber - (this.BlockSize.Width - 1),
                            this.MaxNumber - (this.BlockSize.Height - 1)
                        ),
                        this.MaxNumber
                    );
                    groups.Add(cellGroup4);
                    for (var r = a / this.BlockSize.Width; r < this.MaxNumber; r += this.BlockSize.Height)
                    {
                        for (var c = a % this.BlockSize.Width; c < this.MaxNumber; c += this.BlockSize.Width)
                        {
                            var index = c + r * this.MaxNumber;
                            cellGroup4.AddCell(this.Cells[index]);
                        }
                    }
                }
            }
            if (this.Type == SudokuType.FreeStyle)
            {
                var rects = new Rectangle[this.MaxNumber];
                for (var i = 0; i < this.Cells.Count; i++)
                {
                    var id = this.FreeStyleGroup[i];
                    var cell = this.Cells[i];
                    if (rects[id].Width == 0)
                    {
                        rects[id].Location = cell.CellLocation;
                        rects[id].Width = 1;
                        rects[id].Height = 1;
                    }
                    else
                    {
                        rects[id] = Rectangle.Union(
                            rects[id],
                            new Rectangle(
                                cell.CellLocation.X,
                                cell.CellLocation.Y,
                                1, 1
                            )
                        );
                    }
                }
                var freeGroups = new CellGroup[this.MaxNumber];
                for (var i = 0; i < this.Cells.Count; i++)
                {
                    var id = this.FreeStyleGroup[i];
                    var cellGroup5 = freeGroups[id];
                    if (cellGroup5 == null)
                    {
                        cellGroup5 = CellGroup.GetCellGroup(
                            CellGroup.Type.FreeStyle,
                            rects[id],
                            this.MaxNumber
                        );
                        freeGroups[id] = cellGroup5;
                        groups.Add(cellGroup5);
                    }
                    cellGroup5.AddCell(this.Cells[i]);
                }
            }
            if (this.Type == SudokuType.Diagonal)
            {
                var cellGroup1 = CellGroup.GetCellGroup(
                    CellGroup.Type.Diagonal,
                    new Rectangle(
                        this.CellBounds.X,
                        this.CellBounds.Y,
                        this.MaxNumber,
                        this.MaxNumber
                    ),
                    this.MaxNumber
                );
                var cellGroup2 = CellGroup.GetCellGroup(
                    CellGroup.Type.Diagonal,
                    new Rectangle(
                        this.CellBounds.X + this.MaxNumber - 1,
                        this.CellBounds.Y,
                        this.MaxNumber,
                        this.MaxNumber
                    ),
                    this.MaxNumber
                );
                groups.Add(cellGroup1);
                groups.Add(cellGroup2);
                for (var i = 0; i < this.MaxNumber; i++)
                {
                    var index1 = i + i * this.MaxNumber;
                    cellGroup1.AddCell(this.Cells[index1]);
                    var index2 = (this.MaxNumber - 1 - i) + i * this.MaxNumber;
                    cellGroup2.AddCell(this.Cells[index2]);
                }
            }
            this.CellGroups = groups.AsReadOnly();
            this.hasSetUp = true;
        }

        internal Cell GetCell(Point location)
        {
            if (!this.Bounds.Contains(location))
            {
                return null;
            }
            location -= (Size)this.Bounds.Location;
            location.X /= SudokuFrame.CellSize;
            location.Y /= SudokuFrame.CellSize;
            var index = location.Y * this.MaxNumber + location.X;
            return this.Cells[index];
        }

        internal void DrawBackground(Graphics g)
        {
            switch (this.Type)
            {
                case SudokuType.Diagonal:
                    g.DrawLine(
                        SudokuFrame.DiagonalPen,
                        this.Bounds.Left,
                        this.Bounds.Top,
                        this.Bounds.Right,
                        this.Bounds.Bottom
                    );
                    g.DrawLine(
                        SudokuFrame.DiagonalPen,
                        this.Bounds.Right,
                        this.Bounds.Top,
                        this.Bounds.Left,
                        this.Bounds.Bottom
                    );
                    break;
                case SudokuType.Colored:
                    var state = g.Save();
                    g.TranslateTransform(this.Bounds.X, this.Bounds.Y);
                    for (var r = 0; r < this.MaxNumber; r++)
                    {
                        for (var c = 0; c < this.MaxNumber; c++)
                        {
                            var index = c % this.BlockSize.Width
                                + (r % this.BlockSize.Height) * this.BlockSize.Width;
                            g.FillRectangle(
                                SudokuFrame.CellBrushes[index],
                                c * SudokuFrame.CellSize,
                                r * SudokuFrame.CellSize,
                                SudokuFrame.CellSize,
                                SudokuFrame.CellSize
                            );
                        }
                    }
                    g.Restore(state);
                    break;
                case SudokuType.FreeStyle:
                    var state2 = g.Save();
                    g.TranslateTransform(this.Bounds.X, this.Bounds.Y);
                    for (var i = 0; i < this.FreeStyleGroup.Length; i++)
                    {
                        var r = i / this.MaxNumber;
                        var c = i % this.MaxNumber;
                        var id = this.FreeStyleGroup[i];
                        g.FillRectangle(
                            SudokuFrame.CellBrushes[id],
                            c * SudokuFrame.CellSize,
                            r * SudokuFrame.CellSize,
                            SudokuFrame.CellSize,
                            SudokuFrame.CellSize
                        );
                    }
                    g.Restore(state2);
                    break;
            }
        }

        private void drawNormalFrame(Graphics g)
        {
            var pen1 = Pens.Black;
            var pen2 = SudokuFrame.Black2Pen;

            for (var i = 1; i < this.MaxNumber; i++)
            {
                g.DrawLine(
                    i % this.BlockSize.Height == 0 ? pen2 : pen1,
                    0,
                    i * SudokuFrame.CellSize,
                    this.MaxNumber * SudokuFrame.CellSize,
                    i * SudokuFrame.CellSize
                );
                g.DrawLine(
                    i % this.BlockSize.Width == 0 ? pen2 : pen1,
                    i * SudokuFrame.CellSize,
                    0,
                    i * SudokuFrame.CellSize,
                    this.MaxNumber * SudokuFrame.CellSize
                );
            }

            g.DrawRectangle(
                SudokuFrame.Black3Pen,
                0, 0,
                this.MaxNumber * SudokuFrame.CellSize,
                this.MaxNumber * SudokuFrame.CellSize
            );
        }

        private void drawFreeStyle(Graphics g)
        {
            for (var i = 1; i < this.MaxNumber; i++)
            {
                g.DrawLine(
                    Pens.Black,
                    0,
                    i * SudokuFrame.CellSize,
                    this.MaxNumber * SudokuFrame.CellSize,
                    i * SudokuFrame.CellSize
                );
                g.DrawLine(
                    Pens.Black,
                    i * SudokuFrame.CellSize,
                    0,
                    i * SudokuFrame.CellSize,
                    this.MaxNumber * SudokuFrame.CellSize
                );
            }
            g.DrawRectangle(
                SudokuFrame.Black3Pen,
                0, 0,
                this.Bounds.Width,
                this.Bounds.Height
            );
            for (var r = 0; r < this.MaxNumber; r++)
            {
                for (var c = 1; c < this.MaxNumber; c++)
                {
                    var index1 = (c - 1) + r * this.MaxNumber;
                    var index2 = c + r * this.MaxNumber;
                    if (this.FreeStyleGroup[index1] == this.FreeStyleGroup[index2])
                    {
                        continue;
                    }
                    g.DrawLine(
                        Black2Pen,
                        c * SudokuFrame.CellSize,
                        r * SudokuFrame.CellSize,
                        c * SudokuFrame.CellSize,
                        (r + 1) * SudokuFrame.CellSize
                    );
                }
            }
            for (var c = 0; c < this.MaxNumber; c++)
            {
                for (var r = 1; r < this.MaxNumber; r++)
                {
                    var index1 = c + (r - 1) * this.MaxNumber;
                    var index2 = c + r * this.MaxNumber;
                    if (this.FreeStyleGroup[index1] == this.FreeStyleGroup[index2])
                    {
                        continue;
                    }
                    g.DrawLine(
                        Black2Pen,
                        c * SudokuFrame.CellSize,
                        r * SudokuFrame.CellSize,
                        (c + 1) * SudokuFrame.CellSize,
                        r * SudokuFrame.CellSize
                    );
                }
            }
        }

        internal void Draw(Graphics g)
        {
            var state = g.Save();
            g.TranslateTransform(this.Bounds.X, this.Bounds.Y);

            if (this.Type == SudokuType.FreeStyle)
            {
                this.drawFreeStyle(g);
            }
            else
            {
                this.drawNormalFrame(g);
            }

            g.Restore(state);
        }
    }
}