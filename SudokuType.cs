namespace SudokuSolver
{
    public enum SudokuType
    {
        Normal,
        Union,
        Diagonal,
        Colored,
        FreeStyle
    }
}
