using System;
using System.Windows.Forms;

namespace SudokuSolver
{
    public sealed class Program
    {
        internal const string Name = "数独そるばー";
        internal const string FileExtension = "sspf";

        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }

}