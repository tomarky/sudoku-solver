using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace SudokuSolver
{
    internal sealed class Form3 : Form
    {
        const int CellSize = Form1.CellSize;

        readonly Pen Cursor3Pen = new Pen(Color.Red, 3) {
            Alignment = PenAlignment.Center
        };

        private readonly Size offset = new Size(20, 20);

        private SudokuFrame frame = null;

        private bool moveByKeyboardScrolling = false;

        private Point cursorLocation = new Point(0, 0);
        private int currentFillGroupId = 1;
        private int[] groupCount;
        private bool firstStep = true;

        private Stack<Form3.IEditor> history = new Stack<Form3.IEditor>();

        internal Form3(int maxNumber) : base()
        {
            this.InitializeComponent(maxNumber);
            this.frame = new SudokuFrame(maxNumber);
            this.groupCount = new int[maxNumber + 1];
            this.groupCount[0] = maxNumber * maxNumber;
        }

        internal SudokuFrame Frame
        {
            get
            {
                var res = new SudokuFrame(this.frame.MaxNumber);
                for (var i = 0; i < res.FreeStyleGroup.Length; i++)
                {
                    res.FreeStyleGroup[i] = this.frame.FreeStyleGroup[i] - 1;
                }
                return res;
            }
        }

        private bool isValid
        {
            get
            {
                if (this.groupCount[0] > 0)
                {
                    return false;
                }
                for (var i = 2; i < this.groupCount.Length; i++)
                {
                    if (this.groupCount[i - 1] != this.groupCount[i])
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private void Form3_Shown(object sender, EventArgs e)
        {
            if (this.panel1.CanFocus)
            {
                this.panel1.Focus();
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            this.Size = new Size(450, 500);

            this.pictureBox1.Size = this.frame.Bounds.Size + this.offset + this.offset;

            this.toolStripStatusLabel1.Text = String.Format(
                "第{0}グループに割り当てるセルをクリックしてください",
                this.currentFillGroupId
            );
        }

        private void Form3_SizeChanged(object sender, EventArgs e)
        {
            if (this.panel1.CanFocus)
            {
                this.panel1.Focus();
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (this.frame == null)
            {
                return;
            }
            var g = e.Graphics;
            var state = g.Save();
            g.TranslateTransform(this.offset.Width, this.offset.Height);

            if (this.frame.Bounds.Contains(this.cursorLocation))
            {
                g.DrawLine(
                    Pens.Orange,
                    0,
                    this.cursorLocation.Y,
                    this.pictureBox1.Width - this.offset.Width * 2,
                    this.cursorLocation.Y
                );
                g.DrawLine(
                    Pens.Orange,
                    this.cursorLocation.X,
                    0,
                    this.cursorLocation.X,
                    this.pictureBox1.Height - this.offset.Height * 2
                );
            }

            for (var r = 0; r < this.frame.MaxNumber; r++)
            {
                for (var c = 0; c < this.frame.MaxNumber; c++)
                {
                    var index = c + r * this.frame.MaxNumber;
                    var groupId = this.frame.FreeStyleGroup[index];
                    if (groupId == 0)
                    {
                        continue;
                    }
                    g.FillRectangle(
                        SudokuFrame.CellBrushes[groupId - 1],
                        c * Form3.CellSize,
                        r * Form3.CellSize,
                        Form3.CellSize,
                        Form3.CellSize
                    );
                    g.DrawString(
                        Cell.Numbers[groupId],
                        Cell.font,
                        Brushes.DarkSlateGray,
                        new RectangleF(
                            c * Form3.CellSize,
                            r * Form3.CellSize,
                            Form3.CellSize,
                            Form3.CellSize
                        ),
                        Cell.stringFormat
                    );
                }
            }

            this.frame.Draw(g);

            if (this.frame.Bounds.Contains(this.cursorLocation))
            {
                g.DrawRectangle(
                    Cursor3Pen,
                    this.cursorLocation.X - this.cursorLocation.X % Form3.CellSize,
                    this.cursorLocation.Y - this.cursorLocation.Y % Form3.CellSize,
                    Form3.CellSize,
                    Form3.CellSize
                );
            }

            g.Restore(state);
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (!this.frame.Bounds.Contains(this.cursorLocation))
            {
                return;
            }
            if (e.Button == MouseButtons.Right || !this.firstStep)
            {
                this.contextMenuStrip1.Show(this.pictureBox1, e.Location);
                return;
            }
            var x = this.cursorLocation.X / Form3.CellSize;
            var y = this.cursorLocation.Y / Form3.CellSize;
            var index = x + y * this.frame.MaxNumber;
            var before = this.frame.FreeStyleGroup[index];
            if (before != 0)
            {
                return;
            }
            var ed = new Form3.OneCellEditor(index, this.currentFillGroupId);
            if (!ed.Apply(this.groupCount, this.frame.FreeStyleGroup))
            {
                return;
            }
            this.history.Push(ed);
            this.pictureBox1.Refresh();
            if (this.groupCount[this.currentFillGroupId] == this.frame.MaxNumber)
            {
                this.currentFillGroupId++;
                if (this.currentFillGroupId < this.frame.MaxNumber)
                {
                    this.toolStripStatusLabel1.Text = String.Format(
                        "第{0}グループに割り当てるセルをクリックしてください",
                        this.currentFillGroupId
                    );
                }
                else
                {
                    var ed2 = new Form3.CellsEditor(0, this.currentFillGroupId);
                    if (ed2.Apply(this.groupCount, this.frame.FreeStyleGroup))
                    {
                        this.history.Push(ed2);
                        this.pictureBox1.Refresh();
                    }
                    this.toolStripStatusLabel1.Text = "";
                    this.firstStep = false;
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.moveByKeyboardScrolling)
            {
                this.moveByKeyboardScrolling = false;
                return;
            }
            var oldLocation = this.cursorLocation;
            this.cursorLocation.X = (e.X - this.offset.Width) / Form3.CellSize * Form3.CellSize + Form3.CellSize / 2;
            this.cursorLocation.Y = (e.Y - this.offset.Height) / Form3.CellSize * Form3.CellSize + Form3.CellSize / 2;
            if (oldLocation != this.cursorLocation)
            {
                this.pictureBox1.Refresh();
            }
            if (this.firstStep && e.Button == MouseButtons.Left)
            {
                this.pictureBox1_MouseClick(sender, e);
            }
        }

        private void editMode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                var me = new MouseEventArgs(
                    MouseButtons.Left,
                    1,
                    this.cursorLocation.X + this.offset.Width,
                    this.cursorLocation.Y + this.offset.Height,
                    0
                );
                this.pictureBox1_MouseClick(sender, me);
            }
        }

        private void editMode_KeyDown(object sender, KeyEventArgs e)
        {
            var loc = this.cursorLocation;
            switch (e.KeyCode)
            {
                case Keys.Left:
                    loc.X -= Form3.CellSize;
                    break;
                case Keys.Right:
                    loc.X += Form3.CellSize;
                    break;
                case Keys.Up:
                    loc.Y -= Form3.CellSize;
                    break;
                case Keys.Down:
                    loc.Y += Form3.CellSize;
                    break;
                case Keys.Back:
                    if (this.history.Count > 0)
                    {
                        this.historyBack();
                    }
                    break;
            }
            if (loc == this.cursorLocation)
            {
                return;
            }
            if (!this.frame.Bounds.Contains(loc))
            {
                return;
            }
            this.cursorLocation = loc;
            this.pictureBox1.Refresh();
            this.resetAutoScrollPosition();
        }

        private void resetAutoScrollPosition()
        {
            var position = this.panel1.AutoScrollPosition;
            var rect = new Rectangle(
                -position.X + Form3.CellSize / 2,
                -position.Y + Form3.CellSize / 2,
                this.panel1.ClientSize.Width - Form3.CellSize,
                this.panel1.ClientSize.Height - Form3.CellSize
            );
            if (rect.Contains(this.cursorLocation + this.offset))
            {
                return;
            }
            var moveTo = new Point(
                -position.X,
                -position.Y
            );
            if (this.cursorLocation.X + this.offset.Width < rect.Left)
            {
                moveTo.X = Math.Max(
                    0,
                    moveTo.X - Form3.CellSize * (
                        1 +
                        (rect.Left - (this.cursorLocation.X + this.offset.Width)) /
                        Form3.CellSize
                    )
                );
            }
            if (this.cursorLocation.X + this.offset.Width > rect.Right)
            {
                moveTo.X = Math.Min(
                    this.pictureBox1.Width - this.panel1.ClientSize.Width,
                    moveTo.X + Form3.CellSize * (
                        1 +
                        (this.cursorLocation.X + this.offset.Width - rect.Right) /
                        Form3.CellSize
                    )
                );
            }
            if (this.cursorLocation.Y + this.offset.Height < rect.Top)
            {
                moveTo.Y = Math.Max(
                    0,
                    moveTo.Y - Form3.CellSize * (
                        1 +
                        (rect.Top - (this.cursorLocation.Y + this.offset.Height)) /
                        Form3.CellSize
                    )
                );
            }
            if (this.cursorLocation.Y + this.offset.Height > rect.Bottom)
            {
                moveTo.Y = Math.Min(
                    this.pictureBox1.Height - this.panel1.ClientSize.Height,
                    moveTo.Y + Form3.CellSize * (
                        1 +
                        (this.cursorLocation.Y + this.offset.Height - rect.Bottom) /
                        Form3.CellSize
                    )
                );
            }
            this.moveByKeyboardScrolling = true;
            this.panel1.AutoScrollPosition = moveTo;
        }

        private void cellNumbers_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (!this.frame.Bounds.Contains(this.cursorLocation))
            {
                return;
            }
            var x = this.cursorLocation.X / Form3.CellSize;
            var y = this.cursorLocation.Y / Form3.CellSize;
            var index = x + y * this.frame.MaxNumber;
            var id = (int)e.ClickedItem.Tag;
            var ed = new Form3.OneCellEditor(index, id);
            if (!ed.Apply(this.groupCount, this.frame.FreeStyleGroup))
            {
                return;
            }
            this.history.Push(ed);
            this.pictureBox1.Refresh();
            this.toolStripStatusLabel1.Text = "";
            for (var i = 1; i < this.groupCount.Length; i++)
            {
                if (this.groupCount[i] < this.frame.MaxNumber)
                {
                    this.toolStripStatusLabel1.Text = String.Format(
                        "第{0}グループへの割り当てが不足しています",
                        i
                    );
                    break;
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            this.toolStripLabel1.Enabled = !this.firstStep;
            this.toolStripMenuItem3.Enabled = !this.firstStep;
            this.toolStripMenuItem4.Enabled = !this.firstStep && this.frame.MaxNumber >= 10;
            this.toolStripMenuItem5.Enabled = !this.firstStep && this.frame.MaxNumber >= 20;
            this.toolStripMenuItem6.Enabled = this.history.Count > 0;
        }

        private void toolStripMenuItem1_DropDownOpening(object sender, EventArgs e)
        {
            this.toolStripMenuItem7.Enabled = this.history.Count > 0;
            this.toolStripMenuItem8.Enabled = this.history.Count > 0;
            this.toolStripMenuItem9.Enabled = this.isValid;
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            var ed = new ResetEditor();
            if (ed.Apply(this.groupCount, this.frame.FreeStyleGroup))
            {
                this.history.Push(ed);
                this.pictureBox1.Refresh();
                this.firstStep = true;
                this.currentFillGroupId = 1;
                this.toolStripStatusLabel1.Text = String.Format(
                    "第{0}グループに割り当てるセルをクリックしてください",
                    this.currentFillGroupId
                );
            }
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Hide();
        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Hide();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.toolStripMenuItem2.Enabled = false;
            var form4 = new Form4(this.pictureBox1);
            form4.Text += " for 不定形グループ分け";
            form4.Disposed += (s1, e1) => {
                this.toolStripMenuItem2.Enabled = true;
            };
            form4.Show(this);
        }

        private void historyBack_Click(object sender, EventArgs e)
        {
            this.historyBack();
        }

        private void historyBack()
        {
            var ed = this.history.Pop();
            ed.Apply(this.groupCount, this.frame.FreeStyleGroup);
            this.firstStep = this.groupCount[0] > 0;
            if (this.firstStep)
            {
                for (var i = 1; i < this.groupCount.Length; i++)
                {
                    if (this.groupCount[i] < this.frame.MaxNumber)
                    {
                        this.currentFillGroupId = i;
                        break;
                    }
                }
                if (this.currentFillGroupId == this.frame.MaxNumber && this.history.Count > 0)
                {
                    ed = this.history.Pop();
                    ed.Apply(this.groupCount, this.frame.FreeStyleGroup);
                    this.currentFillGroupId--;
                }
                this.toolStripStatusLabel1.Text = String.Format(
                    "第{0}グループに割り当てるセルをクリックしてください",
                    this.currentFillGroupId
                );
            }
            else if (this.isValid)
            {
                this.toolStripStatusLabel1.Text = "";
            }
            else
            {
                for (var i = 1; i < this.groupCount.Length; i++)
                {
                    if (this.groupCount[i] < this.frame.MaxNumber)
                    {
                        this.toolStripStatusLabel1.Text = String.Format(
                            "第{0}グループへの割り当てが不足しています",
                            i
                        );
                        break;
                    }
                }
            }
            this.pictureBox1.Refresh();
        }

        private void InitializeComponent(int maxNumber)
        {
            this.pictureBox1 = new PictureBox();
            this.panel1 = new Panel();
            this.contextMenuStrip1 = new ContextMenuStrip();
            this.menuStrip1 = new MenuStrip();
            this.statusStrip1 = new StatusStrip();
            this.toolStripStatusLabel1 = new ToolStripStatusLabel();
            this.toolStripLabel1 = new ToolStripLabel();
            this.toolStripMenuItem1 = new ToolStripMenuItem();
            this.toolStripMenuItem2 = new ToolStripMenuItem();
            this.toolStripMenuItem3 = new ToolStripMenuItem();
            this.toolStripMenuItem4 = new ToolStripMenuItem();
            this.toolStripMenuItem5 = new ToolStripMenuItem();
            this.toolStripMenuItem6 = new ToolStripMenuItem();
            this.toolStripMenuItem7 = new ToolStripMenuItem();
            this.toolStripMenuItem8 = new ToolStripMenuItem();
            this.toolStripMenuItem9 = new ToolStripMenuItem();
            this.toolStripMenuItem10 = new ToolStripMenuItem();

            this.panel1.SuspendLayout();
            this.SuspendLayout();

            for (var i = 1; i <= 9; i++)
            {
                var item = this.toolStripMenuItem3.DropDownItems.Add(
                    String.Format("&{0}", i)
                );
                item.Tag = i;
                item.Enabled = i <= maxNumber;
            }
            this.toolStripMenuItem3.Text = "1 - 9(&0)";
            this.toolStripMenuItem3.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.cellNumbers_DropDownItemClicked);

            for (var i = 0; i <= 9; i++)
            {
                var item = this.toolStripMenuItem4.DropDownItems.Add(
                    String.Format("1&{0}", i)
                );
                item.Tag = 10 + i;
                item.Enabled = 10 + i <= maxNumber;
            }
            this.toolStripMenuItem4.Enabled = maxNumber >= 10;
            this.toolStripMenuItem4.Text = "10 - 19(&1)";
            this.toolStripMenuItem4.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.cellNumbers_DropDownItemClicked);

            for (var i = 0; i <= 5; i++)
            {
                var item = this.toolStripMenuItem5.DropDownItems.Add(
                    String.Format("2&{0}", i)
                );
                item.Tag = 20 + i;
                item.Enabled = 20 + i <= maxNumber;
            }
            this.toolStripMenuItem5.Enabled = maxNumber >= 20;
            this.toolStripMenuItem5.Text = "20 - 25(&2)";
            this.toolStripMenuItem5.DropDownItemClicked +=
                new ToolStripItemClickedEventHandler(this.cellNumbers_DropDownItemClicked);

            this.toolStripLabel1.Text = "グループの変更";

            this.toolStripMenuItem6.Text = "操作を取り消す(&D)";
            this.toolStripMenuItem6.Click += new EventHandler(this.historyBack_Click);

            this.contextMenuStrip1.Items.Add(this.toolStripLabel1);
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem3);
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem4);
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem5);
            this.contextMenuStrip1.Items.Add("-");
            this.contextMenuStrip1.Items.Add(this.toolStripMenuItem6);
            this.contextMenuStrip1.Items.Add("-");
            this.contextMenuStrip1.Items.Add("キャンセル (&C)");
            this.contextMenuStrip1.Opening += new CancelEventHandler(this.contextMenuStrip1_Opening);

            this.toolStripMenuItem7.Enabled = false;
            this.toolStripMenuItem7.Text = "操作を取り消す(&D)";
            this.toolStripMenuItem7.Click += new EventHandler(this.historyBack_Click);

            this.toolStripMenuItem8.Enabled = false;
            this.toolStripMenuItem8.Text = "リセット(&R)";
            this.toolStripMenuItem8.Click += new EventHandler(this.toolStripMenuItem8_Click);

            this.toolStripMenuItem9.Enabled = false;
            this.toolStripMenuItem9.Text = "グループ分け完了(&A)";
            this.toolStripMenuItem9.Click += new EventHandler(this.toolStripMenuItem9_Click);

            this.toolStripMenuItem10.Text = "中止(&C)";
            this.toolStripMenuItem10.Click += new EventHandler(this.toolStripMenuItem10_Click);

            this.toolStripMenuItem2.Text = "補助背景(&H)";
            this.toolStripMenuItem2.Click += new EventHandler(this.toolStripMenuItem2_Click);

            this.toolStripMenuItem1.DropDownItems.Add(this.toolStripMenuItem9);
            this.toolStripMenuItem1.DropDownItems.Add("-");
            this.toolStripMenuItem1.DropDownItems.Add(this.toolStripMenuItem7);
            this.toolStripMenuItem1.DropDownItems.Add("-");
            this.toolStripMenuItem1.DropDownItems.Add(this.toolStripMenuItem8);
            this.toolStripMenuItem1.DropDownItems.Add("-");
            this.toolStripMenuItem1.DropDownItems.Add(this.toolStripMenuItem2);
            this.toolStripMenuItem1.DropDownItems.Add("-");
            this.toolStripMenuItem1.DropDownItems.Add(this.toolStripMenuItem10);
            this.toolStripMenuItem1.Text = "メニュー";
            this.toolStripMenuItem1.DropDownOpening +=
                new EventHandler(this.toolStripMenuItem1_DropDownOpening);

            this.menuStrip1.Items.Add(this.toolStripMenuItem1);
            this.menuStrip1.Dock = DockStyle.Top;

            this.statusStrip1.Items.Add(toolStripStatusLabel1);
            this.statusStrip1.Dock = DockStyle.Bottom;

            this.pictureBox1.Size = new Size(400, 400);
            this.pictureBox1.BackColor = Color.White;
            this.pictureBox1.Paint += new PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseClick += new MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.pictureBox1.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Top = this.menuStrip1.Bottom;
            this.panel1.Size = new Size(
                this.ClientSize.Width,
                this.ClientSize.Height -
                this.menuStrip1.Height -
                this.statusStrip1.Height
            );
            this.panel1.AutoScroll = true;
            this.panel1.Anchor =
                AnchorStyles.Left |
                AnchorStyles.Right |
                AnchorStyles.Top |
                AnchorStyles.Bottom;
            this.panel1.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.panel1.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.MainMenuStrip = this.menuStrip1;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.ShowInTaskbar = false;
            this.Text = "グループ分け";
            this.Shown += new EventHandler(this.Form3_Shown);
            this.Load += new EventHandler(this.Form3_Load);
            this.SizeChanged += new EventHandler(this.Form3_SizeChanged);
            this.KeyDown += new KeyEventHandler (this.editMode_KeyDown);
            this.KeyPress += new KeyPressEventHandler (this.editMode_KeyPress);

            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private PictureBox pictureBox1;
        private Panel panel1;
        private ContextMenuStrip contextMenuStrip1;
        private MenuStrip menuStrip1;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripLabel toolStripLabel1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripMenuItem toolStripMenuItem3;
        private ToolStripMenuItem toolStripMenuItem4;
        private ToolStripMenuItem toolStripMenuItem5;
        private ToolStripMenuItem toolStripMenuItem6;
        private ToolStripMenuItem toolStripMenuItem7;
        private ToolStripMenuItem toolStripMenuItem8;
        private ToolStripMenuItem toolStripMenuItem9;
        private ToolStripMenuItem toolStripMenuItem10;

        interface IEditor
        {
            bool Apply(int[] groupCount, int[] freeStyleGroup);
        }

        sealed class OneCellEditor : Form3.IEditor
        {
            int index;
            int id;

            internal OneCellEditor(int index, int id)
            {
                this.index = index;
                this.id = id;
            }

            public bool Apply(int[] groupCount, int[] freeStyleGroup)
            {
                var oldId = freeStyleGroup[this.index];
                if (oldId == this.id)
                {
                    return false;
                }
                groupCount[oldId]--;
                groupCount[this.id]++;
                freeStyleGroup[this.index] = this.id;
                this.id = oldId;
                return true;
            }
        }

        sealed class CellsEditor : Form3.IEditor
        {
            int fromId;
            int toId;

            internal CellsEditor(int fromId, int toId)
            {
                this.fromId = fromId;
                this.toId = toId;
            }

            public bool Apply(int[] groupCount, int[] freeStyleGroup)
            {
                var edited = false;
                for (var i = 0; i < freeStyleGroup.Length; i++)
                {
                    if (freeStyleGroup[i] != this.fromId)
                    {
                        continue;
                    }
                    groupCount[this.fromId]--;
                    groupCount[this.toId]++;
                    freeStyleGroup[i] = this.toId;
                    edited = true;
                }
                if (edited)
                {
                    var tmp = this.fromId;
                    this.fromId = this.toId;
                    this.toId = tmp;
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        sealed class ResetEditor : Form3.IEditor
        {
            int[] groupCount = null;
            int[] freeStyleGroup = null;

            internal ResetEditor() {}

            public bool Apply(int[] groupCount, int[] freeStyleGroup)
            {
                if (this.groupCount == null)
                {
                    if (groupCount[0] == freeStyleGroup.Length)
                    {
                        return false;
                    }
                    this.groupCount = (int[])groupCount.Clone();
                    this.freeStyleGroup = (int[])freeStyleGroup.Clone();
                    Array.Clear(groupCount, 0, groupCount.Length);
                    Array.Clear(freeStyleGroup, 0, freeStyleGroup.Length);
                }
                else
                {
                    this.groupCount.CopyTo(groupCount, 0);
                    this.freeStyleGroup.CopyTo(freeStyleGroup, 0);
                    this.groupCount = null;
                    this.freeStyleGroup = null;
                }
                return true;
            }
        }
    }
}
