using System.Collections.Generic;
using System.Drawing;

namespace SudokuSolver
{
    internal sealed class CellGroup
    {
        internal enum Type
        {
            HorizontalLine,
            VerticalLine,
            Block,
            Diagonal,
            Colored,
            FreeStyle
        }

        private static List<CellGroup> groups = null;

        internal static IList<CellGroup> GetGroups()
        {
            return CellGroup.groups.AsReadOnly();
        }

        internal static void Reset()
        {
            CellGroup.groups = new List<CellGroup>();
        }

        internal static CellGroup GetCellGroup(Rectangle cellBounds)
        {
            var maxNumber = cellBounds.Width * cellBounds.Height;
            if (cellBounds.Width == 1)
            {
                return CellGroup.GetCellGroup(
                    CellGroup.Type.VerticalLine,
                    cellBounds,
                    maxNumber
                );
            }
            else if (cellBounds.Height == 1)
            {
                return CellGroup.GetCellGroup(
                    CellGroup.Type.HorizontalLine,
                    cellBounds,
                    maxNumber
                );
            }
            else
            {
                return CellGroup.GetCellGroup(
                    CellGroup.Type.Block,
                    cellBounds,
                    maxNumber
                );
            }
        }

        internal static CellGroup GetCellGroup(CellGroup.Type groupType, Rectangle cellBounds, int maxNumber)
        {
            foreach (var g in CellGroup.groups)
            {
                if (g.GroupType == groupType && g.CellBounds == cellBounds && g.MaxNumber == maxNumber)
                {
                    return g;
                }
            }
            var index = CellGroup.groups.Count;
            var newGroup = new CellGroup(groupType, index, cellBounds, maxNumber);
            CellGroup.groups.Add(newGroup);
            return newGroup;
        }

        internal IList<Cell> Cells { get; private set; }
        internal readonly int MaxNumber;
        internal readonly int Index;
        internal readonly Rectangle CellBounds;
        internal readonly CellGroup.Type GroupType;

        private CellGroup(CellGroup.Type groupType, int index, Rectangle cellBounds, int maxNumber)
        {
            this.GroupType = groupType;
            this.MaxNumber = maxNumber;
            this.Index = index;
            this.CellBounds = cellBounds;
            this.Cells = new WRList<Cell>(this.MaxNumber);
        }

        internal long FillState()
        {
            var flag = 0L;
            foreach (var cell in this.Cells)
            {
                flag |= 1L << cell.Number;
            }
            return flag;
        }

        internal void AddCell(Cell cell)
        {
            this.Cells.Add(cell);
            cell.AddGroup(this);
        }

        internal int Verify(int oldNumber, int newNumber)
        {
            var invalid = 0;
            foreach (var cell in this.Cells)
            {
                if (oldNumber > 0 && cell.Number == oldNumber)
                {
                    cell.Invalid--;
                }
                if (newNumber > 0 && cell.Number == newNumber)
                {
                    cell.Invalid++;
                    invalid++;
                }
            }
            return invalid;
        }
    }
}
